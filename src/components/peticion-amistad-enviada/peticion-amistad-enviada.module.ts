import { NgModule } from '@angular/core';
import { PeticionAmistadEnviadaComponent } from './peticion-amistad-enviada';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
	declarations: [PeticionAmistadEnviadaComponent],
	imports: [
        IonicModule,
        PipesModule,
        TranslateModule.forChild()],
	exports: [PeticionAmistadEnviadaComponent]
})
export class PeticionAmistadEnviadaModule {}
