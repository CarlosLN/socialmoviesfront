import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { RelacionesUsuariosProvider } from '../../providers/relaciones-usuarios/relUsuarios';
import { TranslateService } from '@ngx-translate/core';
import { Alertas } from '../../providers';

/**
 * Generated class for the PeticionAmistadEnviadaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'peticion-amistad-enviada',
  templateUrl: 'peticion-amistad-enviada.html'
})
export class PeticionAmistadEnviadaComponent {

  @Input('peticion') peticion: any;
  @Output() canceladaEmitter = new EventEmitter<any>();

  traducciones: any;

  constructor(private alertCtrl: AlertController,
    private _relacionesUser: RelacionesUsuariosProvider,
    private translate: TranslateService,
    private alertas: Alertas) {

    this.translate.get('COMPONENTS.PETICION_AMISTAD_ENVIADA').subscribe((value) => {
      this.traducciones = value;
    })
  }

  cancelarPeticion() {
    let mensaje = this.traducciones.CANCELAR_PETICION;

    mensaje = mensaje.replace("{{username}}",this.peticion.username)

    let alert = this.alertCtrl.create({
      title: this.traducciones.CANCELAR_SOLICITUD_ENVIADA,
      message: mensaje,
      buttons: [
        {
          text: this.traducciones.ACEPTAR,
          handler: () => {
            this._relacionesUser.aceptarRechazarCancelarPeticionAmistad(this.peticion.username, 'eliminarAmistad').then((valor)=>{
              this.alertas.mostarAlertaError(this.traducciones.PETICION_CANCELADA, 2000);
              this.canceladaEmitter.emit();
            }).catch((error) => {
              console.log("Error Peticion cancelada",error)
            })
          }
        },
        {
          text: this.traducciones.CANCELAR,
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


  mostrarAlerta(){
    this.alertas.mostarAlerta("Espera a que el usuario acepte tu solicitud para poder acceder a su perfil.");
  }

}
