import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AlertController, Events, NavController } from 'ionic-angular';
import { RelacionesUsuariosProvider } from '../../providers/relaciones-usuarios/relUsuarios';
import { TranslateService } from '@ngx-translate/core';
import { Alertas } from '../../providers';

/**
 * Generated class for the ItemAmigoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'item-amigo',
  templateUrl: 'item-amigo.html'
})
export class ItemAmigoComponent {

  @Input('amistad') amistad: any;
  @Output() amistadEliminadaEmitter = new EventEmitter<any>();

  traducciones: any

  constructor(private alertCtrl: AlertController,
    private _relacionesUser: RelacionesUsuariosProvider,
    private translate: TranslateService,
    private alertas: Alertas,
    public events: Events,
    public navCtrl: NavController) {
    this.translate.get('COMPONENTS.ITEM_AMIGO').subscribe((value) => {
      this.traducciones = value;
    })
  }

  eliminarAmistad() {

    let mensaje = this.traducciones.CONFIRMAR_ELIMINACION.replace("{{username}}", this.amistad.username)

    let alert = this.alertCtrl.create({
      title: this.traducciones.CONFIRMAR_ELIMINACION_TITULO,
      message: mensaje,
      buttons: [
        {
          text: this.traducciones.ACEPTAR,
          handler: () => {
            this._relacionesUser.aceptarRechazarCancelarPeticionAmistad(this.amistad.username, "eliminarAmistad").then((valor) => {
              let mensajeAlerta = this.traducciones.AMISTAD_ELIMINADA.replace("{{username}}",this.amistad.username)
              this.events.publish('eliminarAmigo', this.amistad.username);
              this.alertas.mostarAlerta(mensajeAlerta, 2000);
              this.amistadEliminadaEmitter.emit();
            }).catch((error) => {
              console.log("Error Eliminando amistad", error)
            })
          }
        },
        {
          text: this.traducciones.CANCELAR,
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  irAPerfil() {
    this.navCtrl.push("PerfilUsuarioPage",{
      username: this.amistad.username,
      avatar: this.amistad.avatar,
      tipoUsuario: "amigo"
    });
  }

}
