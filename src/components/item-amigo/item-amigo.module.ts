import { NgModule } from '@angular/core';
import { ItemAmigoComponent } from './item-amigo';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
	declarations: [ItemAmigoComponent],
	imports: [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [ItemAmigoComponent]
})
export class ItemAmigoModule { }
