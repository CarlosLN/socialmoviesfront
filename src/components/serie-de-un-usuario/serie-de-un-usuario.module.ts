import { NgModule } from '@angular/core';
import { SerieDeUnUsuarioComponent } from './serie-de-un-usuario';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [SerieDeUnUsuarioComponent],
	imports: [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [SerieDeUnUsuarioComponent]
})
export class SerieDeUnUsuarioModule {}
