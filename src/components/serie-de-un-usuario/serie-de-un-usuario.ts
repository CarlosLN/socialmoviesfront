import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the SerieDeUnUsuarioComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'serie-de-un-usuario',
  templateUrl: 'serie-de-un-usuario.html'
})
export class SerieDeUnUsuarioComponent {

  @Input('serie') serie: any;

  constructor(public modalCtrl: ModalController) {
    
  }

  abrirSerie() {
    let serieModal = this.modalCtrl.create("SeriePage", { serie: this.serie });
    serieModal.present();
  }

}
