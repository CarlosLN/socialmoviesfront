import { Component, Input } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

/**
 * Generated class for the ItemAmigoComentarioPeliculaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'item-amigo-comentario-pelicula',
  templateUrl: 'item-amigo-comentario-pelicula.html'
})
export class ItemAmigoComentarioPeliculaComponent {

  @Input('amigo') amigoQueHaComentado: any;

  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController) {
    
  }

  irAPerfil() {
    this.navCtrl.push("PerfilUsuarioPage",{
      username: this.amigoQueHaComentado.username,
      avatar: this.amigoQueHaComentado.avatar,
      tipoUsuario: "amigo"
    });
  }

  verComentarioCompleto(){
    let alert = this.alertCtrl.create({
      title: this.amigoQueHaComentado.username,
      cssClass: 'miAlerta',
      message: this.amigoQueHaComentado.comentario,
      buttons: ['Cerrar']
    });
    alert.present();
  }

}
