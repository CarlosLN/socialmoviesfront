import { NgModule } from '@angular/core';
import { ItemAmigoComentarioPeliculaComponent } from './item-amigo-comentario-pelicula';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
	declarations: [ItemAmigoComentarioPeliculaComponent],
	imports: [IonicModule,PipesModule],
	exports: [ItemAmigoComentarioPeliculaComponent]
})
export class ItemAmigoComentarioPeliculaModule {}
