import { NgModule } from '@angular/core';
import { SerieComentadaDeUnUsuarioComponent } from './serie-comentada-de-un-usuario';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [SerieComentadaDeUnUsuarioComponent],
	imports: [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [SerieComentadaDeUnUsuarioComponent]
})
export class SerieComentadaDeUnUsuarioModule {}
