import { Component, Input } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';

/**
 * Generated class for the SerieComentadaDeUnUsuarioComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'serie-comentada-de-un-usuario',
  templateUrl: 'serie-comentada-de-un-usuario.html'
})
export class SerieComentadaDeUnUsuarioComponent {

  @Input('serie') serie: any;

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController) {
  }

  abrirSerie() {
    let serieModal = this.modalCtrl.create("SeriePage", { serie: this.serie });
    serieModal.present();
  }

  verComentarioCompleto() {
    let alert = this.alertCtrl.create({
      title: 'Comentario',
      cssClass: 'miAlerta',
      message: this.serie.comentarioUsuario,
      buttons: ['Cerrar']
    });
    alert.present();
  }

}
