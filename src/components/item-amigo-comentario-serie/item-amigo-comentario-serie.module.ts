import { NgModule } from '@angular/core';
import { ItemAmigoComentarioSerieComponent } from './item-amigo-comentario-serie';
import { PipesModule } from '../../pipes/pipes.module';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [ItemAmigoComentarioSerieComponent],
	imports: [IonicModule,PipesModule],
	exports: [ItemAmigoComentarioSerieComponent]
})
export class ItemAmigoComentarioSerieModule {}
