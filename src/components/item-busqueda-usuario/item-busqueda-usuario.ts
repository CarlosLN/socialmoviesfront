import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { RelacionesUsuariosProvider } from '../../providers/relaciones-usuarios/relUsuarios';
import { TranslateService } from '@ngx-translate/core';
import { Alertas } from '../../providers';

@Component({
  selector: 'item-busqueda-usuario',
  templateUrl: 'item-busqueda-usuario.html'
})
export class ItemBusquedaUsuarioComponent {

  @Input('usuario') usuario: any;
  @Output() peticionEnviadaEmitter = new EventEmitter<any>();

  traducciones: any

  constructor(private alertCtrl: AlertController,
    private _relacionesUser: RelacionesUsuariosProvider,
    private translate: TranslateService,
    private alertas: Alertas) {
    this.translate.get('COMPONENTS.ITEM_BUSQUEDA_USUARIO').subscribe((value) => {
      this.traducciones = value;
    })
  }

  mandarPeticionAmistad(){
    let mensaje = this.traducciones.MANDAR_PETICION_A.replace("{{username}}", this.usuario.username)

    let alert = this.alertCtrl.create({
      title: this.traducciones.MANDAR_PETICION,
      message: mensaje,
      buttons: [
        {
          text: this.traducciones.ACEPTAR,
          handler: () => {
            this._relacionesUser.enviarPeticionAmistad(this.usuario.username).then((valor)=>{
              let mensajeAlerta = this.traducciones.SOLICITUD_ENVIADA.replace("{{username}}",this.usuario.username)
              this.alertas.mostarAlerta(mensajeAlerta, 2000);
              this.peticionEnviadaEmitter.emit();
            }).catch((error)=>{
              console.log("Error mandando peticion de amistad", error)
            })
          }
        },
        {
          text: this.traducciones.CANCELAR,
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

}
