import { NgModule } from '@angular/core';
import { ItemBusquedaUsuarioComponent } from './item-busqueda-usuario';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
	declarations: [ItemBusquedaUsuarioComponent],
	imports:  [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [ItemBusquedaUsuarioComponent]
})
export class ItemBusquedaUsuarioModule {}
