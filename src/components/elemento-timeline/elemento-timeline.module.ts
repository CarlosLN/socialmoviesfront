import { NgModule } from '@angular/core';
import { ElementoTimelineComponent } from './elemento-timeline';
import { PipesModule } from '../../pipes/pipes.module';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [ElementoTimelineComponent],
	imports: [IonicModule,
		PipesModule],
	exports: [ElementoTimelineComponent]
})
export class ElementoTimelineModule {}
