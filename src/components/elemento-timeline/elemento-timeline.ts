import { Component, Input } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';

@Component({
  selector: 'elemento-timeline',
  templateUrl: 'elemento-timeline.html'
})
export class ElementoTimelineComponent {

  @Input('elemento') elemento: any;

  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController) {
    
  }

  irAPerfilMio(elemento) {
    this.navCtrl.push("PerfilUsuarioPage",{
      username: this.elemento.nombreUsuario,
      avatar: this.elemento.imagenUser,
      tipoUsuario: "usuarioActual"
    });
  }

  irAPerfilAmigo(elemento) {
    this.navCtrl.push("PerfilUsuarioPage",{
      username: this.elemento.nombreUsuario,
      avatar: this.elemento.imagenUser,
      tipoUsuario: "amigo"
    });
  }

  irAPelicula(elemento){
    let peliculaModal = this.modalCtrl.create("PeliculaPage", { 
      pelicula: {
        idPelicula: elemento.idSeriePelicula, 
        titulo: elemento.tituloSeriePelicula,
        poster: elemento.poster} });
    peliculaModal.present();
  }

  irASerie(elemento){
    let peliculaModal = this.modalCtrl.create("SeriePage", { 
      serie: {
        idSerie: elemento.idSeriePelicula, 
        titulo: elemento.tituloSeriePelicula,
        poster: elemento.poster} });
    peliculaModal.present();
  }

  verComentarioCompleto(elemento) {
    let alert = this.alertCtrl.create({
      title: 'Comentario',
      cssClass: 'miAlerta',
      message: elemento,
      buttons: ['Cerrar']
    });
    alert.present();
  }

  abrirJuego(element){
    let juego = {idJuego: element.idSeriePelicula,
                  titulo: element.tituloSeriePelicula}
    let peliculaModal = this.modalCtrl.create("JuegoPage", { 
      juego: juego
    });
    peliculaModal.present();
  }

}
