import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

/**
 * Generated class for the VotacionPopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'votacion-popover',
  templateUrl: 'votacion-popover.html'
})
export class VotacionPopoverComponent {

  votos = [
    {
      texto: "No Vista",
      valor: -1
    },
    {
      texto: "10 - Obra Maestra",
      valor: 10
    },
    {
      texto: "9 - Muy Buena",
      valor: 9
    },
    {
      texto: "8 - Notable",
      valor: 8
    },
    {
      texto: "7 - Buena",
      valor: 7
    },
    {
      texto: "6 - Interesante",
      valor: 6
    },
    {
      texto: "5 - Aceptable",
      valor: 5
    },
    {
      texto: "4 - Regular",
      valor: 4
    },
    {
      texto: "3 - Mala",
      valor: 3
    },
    {
      texto: "2 - Muy Mala",
      valor: 2
    },
    {
      texto: "1 - Mejor no verla",
      valor: 1
    }
  ]

  constructor(public viewControler: ViewController) {

  }

  votoClick(voto){
    this.viewControler.dismiss(voto);
  }

}
