import { NgModule } from '@angular/core';
import { VotacionPopoverComponent } from './votacion-popover';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
	declarations: [VotacionPopoverComponent],
	imports: [IonicModule,
        PipesModule,
        TranslateModule.forChild()],
	exports: [VotacionPopoverComponent]
})
export class VotacionPopoverModule {}
