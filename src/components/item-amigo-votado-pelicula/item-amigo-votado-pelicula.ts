import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the ItemAmigoVotadoPeliculaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'item-amigo-votado-pelicula',
  templateUrl: 'item-amigo-votado-pelicula.html'
})
export class ItemAmigoVotadoPeliculaComponent {

  @Input('amigo') amigoQueHaVotado: any;

  constructor(public navCtrl: NavController) {
    
  }

  irAPerfil() {
    this.navCtrl.push("PerfilUsuarioPage",{
      username: this.amigoQueHaVotado.username,
      avatar: this.amigoQueHaVotado.avatar,
      tipoUsuario: "amigo"
    });
  }

}
