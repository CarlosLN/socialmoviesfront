import { NgModule } from '@angular/core';
import { ItemAmigoVotadoPeliculaComponent } from './item-amigo-votado-pelicula';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
	declarations: [ItemAmigoVotadoPeliculaComponent],
	imports: [IonicModule,PipesModule],
	exports: [ItemAmigoVotadoPeliculaComponent]
})
export class ItemAmigoVotadoPeliculaModule {}
