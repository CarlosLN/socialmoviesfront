import { NgModule } from '@angular/core';
import { ItemSerieBuscadaComponent } from './item-serie-buscada';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
	declarations: [ItemSerieBuscadaComponent],
	imports: [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [ItemSerieBuscadaComponent]
})
export class ItemSerieBuscadaModule {}
