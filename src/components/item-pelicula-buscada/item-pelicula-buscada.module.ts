import { NgModule } from '@angular/core';
import { ItemPeliculaBuscadaComponent } from './item-pelicula-buscada';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
	declarations: [ItemPeliculaBuscadaComponent],
	imports: [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [ItemPeliculaBuscadaComponent]
})
export class ItemPeliculaBuscada {}
