import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the ItemPeliculaBuscadaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'item-pelicula-buscada',
  templateUrl: 'item-pelicula-buscada.html'
})
export class ItemPeliculaBuscadaComponent {

  @Input('pelicula') pelicula: any;

  constructor(public modalCtrl: ModalController) {
  }

  abrirPelicula() {
    let peliculaModal = this.modalCtrl.create("PeliculaPage", { pelicula: this.pelicula });
    peliculaModal.present();
  }

}
