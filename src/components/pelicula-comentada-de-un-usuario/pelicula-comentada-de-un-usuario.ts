import { Component, Input } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';

/**
 * Generated class for the PeliculaComentadaDeUnUsuarioComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'pelicula-comentada-de-un-usuario',
  templateUrl: 'pelicula-comentada-de-un-usuario.html'
})
export class PeliculaComentadaDeUnUsuarioComponent {

  @Input('pelicula') pelicula: any;

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController) {
  }

  abrirPelicula() {
    let peliculaModal = this.modalCtrl.create("PeliculaPage", { pelicula: this.pelicula });
    peliculaModal.present();
  }

  verComentarioCompleto() {
    let alert = this.alertCtrl.create({
      title: 'Comentario',
      cssClass: 'miAlerta',
      message: this.pelicula.comentarioUsuario,
      buttons: ['Cerrar']
    });
    alert.present();
  }

}
