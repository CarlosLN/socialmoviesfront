import { NgModule } from '@angular/core';
import { PeliculaComentadaDeUnUsuarioComponent } from './pelicula-comentada-de-un-usuario';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [PeliculaComentadaDeUnUsuarioComponent],
	imports: [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [PeliculaComentadaDeUnUsuarioComponent]
})
export class PeliculaComentadaDeUnUsuarioModule {}
