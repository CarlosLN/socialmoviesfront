import { IonicModule } from 'ionic-angular';
import { NgModule } from '../../../node_modules/@angular/core';
import { PeticionAmistadRecibidaComponent } from './peticion-amistad-recibida';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
    declarations: [
        PeticionAmistadRecibidaComponent,
    ],
    imports: [
        IonicModule,
        PipesModule,
        TranslateModule.forChild()
    ],
    exports: [
        PeticionAmistadRecibidaComponent
    ]
})

export class PeticionAmistadRecibidaModule { }