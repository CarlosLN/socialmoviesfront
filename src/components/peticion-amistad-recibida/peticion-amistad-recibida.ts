import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { RelacionesUsuariosProvider } from '../../providers/relaciones-usuarios/relUsuarios';
import { TranslateService } from '@ngx-translate/core';
import { Alertas } from '../../providers/global/alertas';

@Component({
  selector: 'peticion-amistad-recibida',
  templateUrl: 'peticion-amistad-recibida.html'
})
export class PeticionAmistadRecibidaComponent {

  @Input('peticion') peticion: any;
  @Output() aceptadaRechazadaEmitter = new EventEmitter<any>();

  traducciones: any;

  constructor(private alertCtrl: AlertController,
    private _relacionesUser: RelacionesUsuariosProvider,
    private translate: TranslateService,
    private alertas: Alertas,
    public navCtrl: NavController) {

    this.translate.get('COMPONENTS.PETICION_AMISTAD_RECIBIDA').subscribe((value) => {
      this.traducciones = value;
    })
  }

  aceptarDenegarPeticion(accion: string) {
    let mensaje = ""

    if (accion == "aceptarPeticion") {
      mensaje = this.traducciones.ACEPTAR_PETICION
    } else {
      mensaje = this.traducciones.RECHAZAR_PETICION
    }

    mensaje = mensaje.replace("{{username}}", this.peticion.username)

    let alert = this.alertCtrl.create({
      title: this.traducciones.SOLICITUD_AMISTAD,
      message: mensaje,
      buttons: [
        {
          text: this.traducciones.ACEPTAR,
          handler: () => {
            this._relacionesUser.aceptarRechazarCancelarPeticionAmistad(this.peticion.username, accion).then((valor) => {
              if (accion == "aceptarPeticion") {
                this.alertas.mostarAlerta(this.traducciones.PETICION_ACEPTADA.replace('{{username}}', this.peticion.username), 2000);
              } else {
                this.alertas.mostarAlertaError(this.traducciones.PETICION_RECHAZADA, 2000);
              }
              this.aceptadaRechazadaEmitter.emit();
            }).catch((error) => {
              console.log("Error Peticion aceptada", error)
            })
          }
        },
        {
          text: this.traducciones.CANCELAR,
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  irAPerfil() {
    this.navCtrl.push("PerfilUsuarioPage",{
      username: this.peticion.username,
      avatar: this.peticion.avatar,
      tipoUsuario: "peticionAmistadRecibida"
    });
  }

}
