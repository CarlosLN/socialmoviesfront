import { NgModule } from '@angular/core';
import { ItemAmigoVotadoSerieComponent } from './item-amigo-votado-serie';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
	declarations: [ItemAmigoVotadoSerieComponent],
	imports: [IonicModule,PipesModule],
	exports: [ItemAmigoVotadoSerieComponent]
})
export class ItemAmigoVotadoSerieModule {}
