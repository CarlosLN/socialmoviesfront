import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'item-amigo-votado-serie',
  templateUrl: 'item-amigo-votado-serie.html'
})
export class ItemAmigoVotadoSerieComponent {

  @Input('amigo') amigoQueHaVotado: any;

  constructor(public navCtrl: NavController) {

  }

  irAPerfil() {
    this.navCtrl.push("PerfilUsuarioPage",{
      username: this.amigoQueHaVotado.username,
      avatar: this.amigoQueHaVotado.avatar,
      tipoUsuario: "amigo"
    });
  }

}
