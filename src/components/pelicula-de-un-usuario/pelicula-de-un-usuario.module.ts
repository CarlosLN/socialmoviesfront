import { NgModule } from '@angular/core';
import { PeliculaDeUnUsuarioComponent } from './pelicula-de-un-usuario';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [PeliculaDeUnUsuarioComponent],
	imports: [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [PeliculaDeUnUsuarioComponent]
})
export class PeliculaDeUnUsuarioModule {}
