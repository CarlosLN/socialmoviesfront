import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the PeliculaDeUnUsuarioComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'pelicula-de-un-usuario',
  templateUrl: 'pelicula-de-un-usuario.html'
})
export class PeliculaDeUnUsuarioComponent {

  @Input('pelicula') pelicula: any;

  constructor(public modalCtrl: ModalController) {
  }

  abrirPelicula() {
    let peliculaModal = this.modalCtrl.create("PeliculaPage", { pelicula: this.pelicula });
    peliculaModal.present();
  }

}
