import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the ItemPropioUsuarioComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'item-propio-usuario',
  templateUrl: 'item-propio-usuario.html'
})
export class ItemPropioUsuarioComponent {

  @Input('peticion') peticion: any;

  constructor(public navCtrl: NavController) {
    
  }

  irAPerfil(){
    this.navCtrl.push("PerfilUsuarioPage", {
      username: this.peticion.username,
      avatar: this.peticion.avatar,
      tipoUsuario: "usuarioActual"
    });
  }

}
