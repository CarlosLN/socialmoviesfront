import { NgModule } from '@angular/core';
import { ItemPropioUsuarioComponent } from './item-propio-usuario';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [ItemPropioUsuarioComponent],
	imports: [IonicModule,
		PipesModule,
		TranslateModule.forChild()],
	exports: [ItemPropioUsuarioComponent]
})
export class ItemPropioUsuarioModule {}
