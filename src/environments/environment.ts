export const environment = {
    production: false,
    host:"http://192.168.1.38:8080/",
    endpoint: "socialmovies/v1/",
    usuarios: {
        login: "usuarios/login",
        registro: "usuarios",
        validarUsuario: "usuarios/usuario/{{username}}",
        actualizarUsuario: "usuarios/usuario/{{username}}",
        cambiarPassword: "usuarios/usuario/{{username}}/cambiarPassword",
        darDeBajaUnUsuario: "usuarios/usuario/{{username}}/darDeAltaBaja/baja",
        obtenerTimeLine: "usuarios/{{username}}/timeLine/{{cuando}}"
    },
    relaciones_usuarios:{
        obtenerAmistades: "usuarios/{{username}}/obtenerAmigos",
        obtenerPeticionesAmistad: "usuarios/{{username}}/peticionesAmistad",
        obtenerPeticionesEnviadas: "usuarios/{{username}}/peticionesAmistadEnviadas",
        aceptarRechazarEliminarPeticionAmistad: "usuarios/{{username}}/peticionesAmistad",
        busquedaUsuario: "usuarios/{{username}}/busquedaUsuario/{{usernameBuscado}}",
        obtenerListadoAmigosDeUnAmigo: "usuarios/{{username}}/obtenerListadoDeAmigosDeUnAmigo/{{usernameAmigo}}",
        enviarPeticionAmistad: "usuarios/{{username}}/peticionesAmistad",
        obtenerPerfilUsuario: "usuarios/{{username}}/devolverPerfilUsuario/{{usuarioSolicitado}}",
        obtenerDatosPerfilAmigo: "usuarios/{{username}}/devolverDatosPerfilAmigo/{{usernameAmigo}}"
    },
    peliculas:{
        buscarPelicula: "peliculas/buscarPelicula/{{peliculaBuscada}}",
        devolverPeliculaUsuario: "peliculas/devolverPeliculaUsuario/{{idPelicula}}/usuario/{{username}}",
        obtenerPeliculaRecomendada: "usuarios/{{username}}/obtenerPeliculaRecomendada/{{idPelicula}}",
        obtenerPeliculaMejorValorada: "peliculas/obtenerPeliculaMejorValorada/usuario/{{username}}",
        obtenerPeliculaMasVotada:"peliculas/obtenerPeliculaMasVotada/usuario/{{username}}",
        //Votos
        obtenerPeliculasVotadas: "usuarios/{{username}}/obtenerVotacionesPeliculasAmigo/{{usernameAmigo}}",
        votarPelicula: "usuarios/{{username}}/votarPelicula/{{idPelicula}}",
        devolverVotacionesAmigosEnFichaDePelicula: "peliculas/devolverVotacionesAmigosEnFichaDePelicula/{{idPelicula}}/usuario/{{username}}",
        //Comentarios
        obtenerPeliculasComentadas: "usuarios/{{username}}/obtenerComentariosPeliculasAmigo/{{usernameAmigo}}",
        devolverComentariosAmigosEnFichaDePelicula: "peliculas/devolverComentariosAmigosEnFichaDePelicula/{{idPelicula}}/usuario/{{username}}",
        comentarPelicula: "usuarios/{{username}}/comentarPelicula/{{idPelicula}}"
    },
    series:{
        buscarSerie: "series/buscarSerie/{{serieBuscada}}",
        devolverSerieUsuario: "series/devolverSerieUsuario/{{idSerie}}/usuario/{{username}}",
        devolverTemporadaYSusCapitulosDeUnaSerie: "series/devolverTemporadaYSusCapitulosDeUnaSerie/{{idSerie}}/temporada/{{temporada}}/usuario/{{username}}",
        obtenerSerieRecomendada: "usuarios/{{username}}/obtenerSerieRecomendada/{{idSerie}}",
        obtenerSerieMejorValorada: "series/obtenerSerieMejorValorada/usuario/{{username}}",
        obtenerSerieMasVotada: "series/obtenerSerieMasVotada/usuario/{{username}}",
        //Votos
        obtenerSeriesVotadas:"usuarios/{{username}}/obtenerVotacionesSeriesAmigo/{{usernameAmigo}}",
        votarCapituloSerie: "usuarios/{{username}}/votarCapitulo/{{idCapitulo}}",
        devolverVotacionesAmigosEnFichaDeSerie: "series/devolverVotacionesAmigosEnFichaDeSerie/{{idSerie}}/usuario/{{username}}",
        //Comentarios
        obtenerSeriesComentadas: "usuarios/{{username}}/obtenerComentariosCapitulosSeriesAmigo/{{usernameAmigo}}",
        devolverComentariosAmigosEnFichaDeSerie: "series/devolverComentariosAmigosEnFichaCapituloSerie/{{idSerie}}/usuario/{{username}}",
        comentarCapituloSerie: "usuarios/{{username}}/comentarCapituloSerie/{{idCapituloSerie}}"
    },
    juegos:{
        participarEnJuego: "usuarios/{{username}}/participarEnJuego",
        obtenerListadoDeJuegosConMisParticipaciones: "juegos/devolverTodosLosJuegos/usuario/{{username}}",
        obtenerJuegoParaElUsuario: "juegos/devolverJuego/{{idJuego}}/usuario/{{username}}",
        obtenerRankingTotalDeUsuariosEnJuegos: "juegos/devolverRakingTotaldeUsuariosEnJuegos"
    }
};