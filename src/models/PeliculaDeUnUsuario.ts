class PeliculaDeUnUsuario{

    id: number;
    titulo: String;
    sinopsis: String;
    fechaEstreno: Date;
    direccion: String;
    reparto: String;
    poster: String;
    duracion: String;
    trailer: String;
    genero: String;
    votacionMedia: number;
    numVotantes: number;
    tuVoto: number;
    votacionesAmigos: any;

    constructor(){
        
    }
}