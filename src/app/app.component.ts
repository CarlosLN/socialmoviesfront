import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform, AlertController, Events } from 'ionic-angular';

import { WelcomePage, MainPage } from '../pages';
import { Storage } from '@ionic/storage';
import { User, Alertas } from '../providers';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Inicio', component: 'HomePage', icono: 'ios-home-outline' },
    { title: 'Mi perfil', component: 'PerfilUsuarioPage', icono: 'ios-contact-outline'},
    { title: 'Amigos', component: 'RelacionesUsuarioPage', icono: 'ios-contacts-outline'},
    { title: 'Películas', component: 'PeliculasPage', icono: 'ios-film-outline'},
    { title: 'Series', component: 'SeriesPage', icono: 'ios-desktop-outline'},
    { title: 'Juegos', component: 'JuegosPage', icono: 'ios-game-controller-b-outline'},
    { title: 'Editar Perfil', component: 'AjustesPerfilUsuarioPage', icono: 'ios-construct-outline'}
  ]
  traduccionesMenu: any;
  userSession =
    {
      avatar: "",
      username: ""
    };

  constructor(
    private translate: TranslateService,
    platform: Platform,
    private config: Config,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private storage: Storage,
    private user: User,
    private alertCtrl: AlertController,
    public events: Events,
    private alertas: Alertas) {
    platform.ready().then(() => {

      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      this.obtenerSesion();

    });
    this.initTranslate();

    events.subscribe('actualizar:user', (user) => {
      console.log("Actualizando usuario", user.getUsername())
      this.userSession.avatar = user.getAvatar()
      this.userSession.username = user.getUsername()
    });

    events.subscribe('actualizar:avatar', (avatar) => {
      this.userSession.avatar = avatar
    });
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('es');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('es'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });

    this.translate.get('PAGINAS.MENU').subscribe((value) => {
      this.traduccionesMenu = value;
    })
  }

  obtenerSesion() {
    this.storage.get('sesion').then((sesion) => {

      //TODO HACER LOGIN PARA VERIFICAR QUE EL USUARIO AUN ESTÁ ACTIVO
      
      if (sesion == null) {
        this.rootPage = WelcomePage
      } else {
        this.user.esValido(sesion).then((valor) => {
          if (sesion == null) {
            this.rootPage = WelcomePage
          } else {
            this.rootPage = MainPage
            this.user.setSesion(sesion);
            this.userSession = this.user.getSesion();
          }
        }).catch(error => {
          console.log(error)
          this.rootPage = WelcomePage
          this.alertas.mostarAlertaError(this.traduccionesMenu.ERR_USUARIO_NO_VALIDO);
        })
      }
    }).catch(error => {
      console.log("Error app component", error)
    })
  }

  openPage(page) {
    if(page.component=="PerfilUsuarioPage"){
      this.irPerfil();
    }else if (page.component=="AjustesPerfilUsuarioPage"){
      this.nav.push("AjustesPerfilUsuarioPage", {
        username: this.userSession.username,
        avatar: this.userSession.avatar
      });
    }else if (page.component=="HomePage"){
      this.nav.popToRoot()
    }else{
      this.nav.push(page.component);
    }
  }

  cerrarSesion() {
    let alert = this.alertCtrl.create({
      title: this.traduccionesMenu.CERRAR_SESION,
      message: this.traduccionesMenu.CERRAR_SESION_PREG,
      buttons: [
        {
          text: this.traduccionesMenu.ACEPTAR,
          handler: () => {
            this.user.logout();
            this.nav.setRoot('WelcomePage');
          }
        },
        {
          text: this.traduccionesMenu.CANCELAR,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  irPerfil() {
    this.nav.push("PerfilUsuarioPage", {
      username: this.userSession.username,
      avatar: this.userSession.avatar,
      tipoUsuario: "usuarioActual"
    });
  }
}
