import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicErrorHandler, IonicModule, ActionSheetController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { Settings, User, Alertas } from '../providers';
import { MyApp } from './app.component';
import { PipesModule } from '../pipes/pipes.module';
import { RelacionesUsuariosProvider } from '../providers/relaciones-usuarios/relUsuarios';
import { PeliculasProvider } from '../providers/peliculas/peliculas';
import { VotacionPopoverComponent } from '../components/votacion-popover/votacion-popover';
import { VotacionPopoverModule } from '../components/votacion-popover/votacion-popover.module';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SeriesProvider } from '../providers/series/series';
import { JuegosProvider } from '../providers/juegos/juegos';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    PipesModule,
    IonicImageViewerModule,
    HttpClientModule,
    VotacionPopoverModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    VotacionPopoverComponent
  ],
  providers: [
    User,
    Alertas,
    ActionSheetController,
    HTTP,
    Camera,
    SplashScreen,
    StatusBar,
    InAppBrowser,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RelacionesUsuariosProvider,
    PeliculasProvider,
    SeriesProvider,
    JuegosProvider
  ]
})
export class AppModule { }
