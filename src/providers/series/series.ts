import { Injectable } from '@angular/core';
import { User } from '..';
import { HTTP } from '@ionic-native/http';
import { environment } from '../../environments/environment';

/*
  Generated class for the SeriesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SeriesProvider {

  constructor(private http: HTTP,
    private userSession: User) {
  }

  buscarSeries(serieBuscada: string, pagina: number, cuantasPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + pagina,
      'cuantasSeries': '' + cuantasPorPagina
    }

    let url = environment.host + environment.endpoint + environment.series.buscarSerie.
      replace("{{serieBuscada}}", serieBuscada)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerSerieUsuario(idSerie: string) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'cuantosAmigosPorVotosYComentarios': '' + 3
    }

    let url = environment.host + environment.endpoint + environment.series.devolverSerieUsuario.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idSerie}}", idSerie)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerTemporadaYSusCapitulosDeUnaSerie(idSerie: string, temporada: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      
    }

    let url = environment.host + environment.endpoint + environment.series.devolverTemporadaYSusCapitulosDeUnaSerie.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idSerie}}", idSerie).
      replace("{{temporada}}", ''+temporada)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  votarCapitulo(idCapitulo, voto) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let body = {
      voto: voto
    }

    let url = environment.host + environment.endpoint +
      environment.series.votarCapituloSerie.
        replace("{{username}}", this.userSession.getUsername()).
        replace("{{idCapitulo}}", idCapitulo);

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header);
  }

  obtenerSerieRecomendada(idSerie) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + environment.series.obtenerSerieRecomendada.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idSerie}}", idSerie)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerSerieMejorValorada(pagina, cuantosPorPagina) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + pagina,
      'cuantosPorPagina': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.series.obtenerSerieMejorValorada.
      replace("{{username}}", this.userSession.getUsername())

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerSerieMasVotada(pagina, cuantosPorPagina) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + pagina,
      'cuantosPorPagina': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.series.obtenerSerieMasVotada.
      replace("{{username}}", this.userSession.getUsername())

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

   /**
   * 
   * @param idPleicula 
   * @param paginaAmigosVotacion 
   */
  obtenerVotacionesDeAmigosDeUnaSerie(idSerie, paginaAmigosVotacion: number, cuantosPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + paginaAmigosVotacion,
      'cuantosAmigos': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.series.devolverVotacionesAmigosEnFichaDeSerie.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idSerie}}", idSerie)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  /**
   * 
   * @param nombreUsuario 
   * @param orden 
   * @param pagina 
   */
  obtenerSeriesVotadasDeUnUsuario(nombreUsuario: string, orden: string, pagina: number, cuantosPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'orden': orden,
      'pagina': '' + pagina,
      'cuantasSeries': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.series.obtenerSeriesVotadas.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{usernameAmigo}}", nombreUsuario)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }
  

  /**
   * 
   * @param idCapitulo 
   * @param comentario 
   */
  comentarCapituloSerie(idCapitulo, comentario) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let body = {
      comentario: comentario
    }

    let url = environment.host + environment.endpoint +
      environment.series.comentarCapituloSerie.
        replace("{{username}}", this.userSession.getUsername()).
        replace("{{idCapituloSerie}}", idCapitulo);

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header);
  }


  obtenerCapituloSerieComentadosDeUnUsuario(nombreUsuario: string, orden: string, pagina: number, cuantosPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'orden': orden,
      'pagina': '' + pagina,
      'cuantasSeries': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.series.obtenerSeriesComentadas.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{usernameAmigo}}", nombreUsuario)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }


  obtenerComentariosDeAmigosDeUnaSerie(idSerie, paginaAmigosComentarios: number, cuantosPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + paginaAmigosComentarios,
      'cuantosAmigos': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.series.devolverComentariosAmigosEnFichaDeSerie.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idSerie}}", idSerie)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

}
