import { Injectable } from '@angular/core';
import { User } from '../user/user';
import { HTTP } from '@ionic-native/http';
import { environment } from '../../environments/environment';

/*
  Generated class for the JuegosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class JuegosProvider {

  constructor(private http: HTTP,
    private userSession: User) {
      
  }

  participarEnJuego(respuestas){
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let body = respuestas

    let url = environment.host + environment.endpoint +
      environment.juegos.participarEnJuego.
        replace("{{username}}", this.userSession.getUsername())

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header);
  }

  obtenerListadoDeJuegosConMisParticipaciones(){
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + 
    environment.juegos.obtenerListadoDeJuegosConMisParticipaciones.
      replace("{{username}}", this.userSession.getUsername())

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerJuegoParaElUsuario(idJuego){
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + 
    environment.juegos.obtenerJuegoParaElUsuario.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idJuego}}", idJuego)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerRankingTotalDeUsuariosEnJuegos(pagina: number, cuantosPorPagina: number){
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + pagina,
      'cuantosPorPagina': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + 
    environment.juegos.obtenerRankingTotalDeUsuariosEnJuegos

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

}
