import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage'
import { environment } from '../../environments/environment';

import * as sha512 from 'js-sha512';


@Injectable()
export class User {
  private sesion: userSesion;

  constructor(public http: HTTP,
    public storage: Storage) {
    this.sesion = {
      token: "",
      username: "",
      avatar: ""
    }
  }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    var url = environment.host + environment.endpoint + environment.usuarios.login
    var body = {
      "username": accountInfo.username,
      "password": sha512.sha512(accountInfo.password)
    }
    var header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header)
  }

  esValido(sesion) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sesion.token
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + environment.usuarios.validarUsuario.
      replace("{{username}}", sesion.username)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    var header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

    var body = {
      "username": accountInfo.username,
      "password": sha512.sha512(accountInfo.password),
      "email": accountInfo.email,
      "avatar": accountInfo.avatar
    }

    var url = environment.host + environment.endpoint + environment.usuarios.registro

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header)
  }

  obtenerPerfilUsuario(usuarioSolicitado: string) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + environment.relaciones_usuarios.obtenerPerfilUsuario.
      replace("{{username}}", this.getUsername()).
      replace("{{usuarioSolicitado}}", usuarioSolicitado);

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);

  }

  /**
   * 
   * @param campo Puede ser avatar, email
   * @param valor 
   */
  actualizarUsuario(campo: string, valor: string) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.getToken()
    }

    let body = {
      campo: campo,
      valor: valor
    }

    let url = environment.host + environment.endpoint +
      environment.usuarios.actualizarUsuario.
        replace("{{username}}", this.getUsername());

    this.http.setDataSerializer('json')
    return this.http.patch(url, body, header);
  }

  cambiarPassword(passNueva: string, passAntigua: string) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.getToken()
    }

    let body = {
      valor: sha512.sha512(passNueva),
      oldpassword: sha512.sha512(passAntigua)
    }

    let url = environment.host + environment.endpoint +
      environment.usuarios.cambiarPassword.
        replace("{{username}}", this.getUsername());

    this.http.setDataSerializer('json')
    return this.http.patch(url, body, header);
  }

  obtenerTimeLine(fechaPrimerElemento:any , fechaUltimoElemento:any, cuando:any){
    var header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.getToken()
    }

    var body = {}

    if(fechaPrimerElemento!=null){
      body = {
        fechaPrimerElemento: "" + fechaPrimerElemento,
        fechaUltimoElemento: "" + fechaUltimoElemento
      }
    }

    var url = environment.host + environment.endpoint + environment.usuarios.obtenerTimeLine.
      replace("{{username}}", this.getUsername()).
      replace("{{cuando}}", cuando);

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header)
  }

  darDeBajaUnUsuario(){
    var header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.getToken()
    }

    var body = {};

    var url = environment.host + environment.endpoint + environment.usuarios.darDeBajaUnUsuario.
      replace("{{username}}", this.getUsername());

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header)
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    //this.sesion = null;
    this.storage.set("sesion", null);
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this.sesion = resp.user;
  }

  setSesion(sesion) {
    this.sesion = sesion
    this.storage.set("sesion", this.sesion);
  }

  getSesion() {
    return this.sesion;
  }

  setToken(token: string) {
    this.sesion.token = token;
    this.storage.set("sesion", this.sesion);
  }

  getToken(): string {
    return this.sesion.token;
  }

  setUsername(username: string) {
    this.sesion.username = username;
    this.storage.set("sesion", this.sesion);
  }

  getUsername(): string {
    return this.sesion.username;
  }

  setAvatar(avatar: string) {
    this.sesion.avatar = avatar;
    this.storage.set("sesion", this.sesion);
  }

  getAvatar(): string {
    return this.sesion.avatar;
  }
}

export interface userSesion {
  token: string,
  username: string,
  avatar: string
}
