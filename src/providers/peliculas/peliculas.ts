import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { User } from '..';
import { environment } from '../../environments/environment';

/*
  Generated class for the PeliculasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PeliculasProvider {

  constructor(private http: HTTP,
    private userSession: User) {
  }

  /**
   * 
   * @param nombreUsuario 
   * @param orden 
   * @param pagina 
   */
  obtenerPeliculasVotadasDeUnUsuario(nombreUsuario: string, orden: string, pagina: number, cuantosPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'orden': orden,
      'pagina': '' + pagina,
      'cuantasPeliculas': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.peliculas.obtenerPeliculasVotadas.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{usernameAmigo}}", nombreUsuario)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  buscarPelicula(peliculaBuscada: string, pagina: number, cuantasPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + pagina,
      'cuantasPeliculas': '' + cuantasPorPagina
    }

    let url = environment.host + environment.endpoint + environment.peliculas.buscarPelicula.
      replace("{{peliculaBuscada}}", peliculaBuscada)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  /**
   * 
   * @param nombreUsuario 
   * @param orden 
   * @param pagina 
   * @param cuantosPorPagina 
   */
  obtenerPeliculasComentadasDeUnUsuario(nombreUsuario: string, orden: string, pagina: number, cuantosPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'orden': orden,
      'pagina': '' + pagina,
      'cuantasPeliculas': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.peliculas.obtenerPeliculasComentadas.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{usernameAmigo}}", nombreUsuario)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  /**
   * 
   * @param idPleicula 
   * @param paginaAmigosVotacion 
   */
  obtenerVotacionesDeAmigosDeUnaPelicula(idPelicula, paginaAmigosVotacion: number, cuantosPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + paginaAmigosVotacion,
      'cuantosAmigos': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.peliculas.devolverVotacionesAmigosEnFichaDePelicula.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idPelicula}}", idPelicula)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  /**
   * 
   * @param idPleicula 
   * @param paginaAmigosVotacion 
   */
  obtenerComentariosDeAmigosDeUnaPelicula(idPelicula, paginaAmigosComentarios: number, cuantosPorPagina: number) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + paginaAmigosComentarios,
      'cuantosAmigos': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.peliculas.devolverComentariosAmigosEnFichaDePelicula.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idPelicula}}", idPelicula)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  /**
   * 
   * @param idPelicula 
   */
  obtenerPeliculaUsuario(idPelicula: string) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + environment.peliculas.devolverPeliculaUsuario.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idPelicula}}", idPelicula)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerPeliculaRecomendada(idPelicula) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + environment.peliculas.obtenerPeliculaRecomendada.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{idPelicula}}", idPelicula)

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerPeliculaMejorValorada(pagina, cuantosPorPagina) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + pagina,
      'cuantosPorPagina': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.peliculas.obtenerPeliculaMejorValorada.
      replace("{{username}}", this.userSession.getUsername())

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerPeliculaMasVotada(pagina, cuantosPorPagina) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + pagina,
      'cuantosPorPagina': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.peliculas.obtenerPeliculaMasVotada.
      replace("{{username}}", this.userSession.getUsername())

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  /**
   * 
   * @param idPelicula 
   * @param voto 
   */
  votarPelicula(idPelicula, voto) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let body = {
      voto: voto
    }

    let url = environment.host + environment.endpoint +
      environment.peliculas.votarPelicula.
        replace("{{username}}", this.userSession.getUsername()).
        replace("{{idPelicula}}", idPelicula);

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header);
  }

  /**
   * 
   * @param idPelicula 
   * @param comentario 
   */
  comentarPelicula(idPelicula, comentario) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let body = {
      comentario: comentario
    }

    let url = environment.host + environment.endpoint +
      environment.peliculas.comentarPelicula.
        replace("{{username}}", this.userSession.getUsername()).
        replace("{{idPelicula}}", idPelicula);

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header);
  }

}
