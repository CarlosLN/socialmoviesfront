import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { User } from '../user/user';
import { environment } from '../../environments/environment';

@Injectable()
export class RelacionesUsuariosProvider {

  constructor(private http: HTTP,
    private userSession: User) {

  }

  obtenerAmistades() {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + environment.relaciones_usuarios.obtenerAmistades.
      replace("{{username}}", this.userSession.getUsername());

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerPeticionesAmistad() {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {

    }

    let url = environment.host + environment.endpoint +
      environment.relaciones_usuarios.obtenerPeticionesAmistad.
        replace("{{username}}", this.userSession.getUsername());

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerPeticionesAmistadEnviadas(){
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {

    }

    let url = environment.host + environment.endpoint +
      environment.relaciones_usuarios.obtenerPeticionesEnviadas.
        replace("{{username}}", this.userSession.getUsername());

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  /**
   * 
   * @param usuario 
   * @param accion aceptarPeticion | rechazarPeticion | eliminarAmistad
   */
  aceptarRechazarCancelarPeticionAmistad(usuario: string, accion: string) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let body = {
      accion: accion,
      usernameAmigo: usuario
    }

    let url = environment.host + environment.endpoint +
      environment.relaciones_usuarios.aceptarRechazarEliminarPeticionAmistad.
        replace("{{username}}", this.userSession.getUsername());

    this.http.setDataSerializer('json')
    return this.http.patch(url, body, header);

  }

  enviarPeticionAmistad(usuario: string) {
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let body = {
      usernameSolicitado: usuario
    }

    let url = environment.host + environment.endpoint +
      environment.relaciones_usuarios.enviarPeticionAmistad.
        replace("{{username}}", this.userSession.getUsername());

    this.http.setDataSerializer('json')
    return this.http.post(url, body, header);
  }

  buscarUsuario(usuarioBuscado: string){
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + environment.relaciones_usuarios.busquedaUsuario.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{usernameBuscado}}", usuarioBuscado);

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerListadoAmigosDeUnAmigo(usuarioAmigo: string, pagina: number, cuantosPorPagina: number){
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
      'pagina': '' + pagina,
      'cuantosPorPagina': '' + cuantosPorPagina
    }

    let url = environment.host + environment.endpoint + environment.relaciones_usuarios.obtenerListadoAmigosDeUnAmigo.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{usernameAmigo}}", usuarioAmigo);

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }

  obtenerDatosPerfilAmigo(usernameAmigo: string){
    let header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.userSession.getToken()
    }

    let params = {
    }

    let url = environment.host + environment.endpoint + environment.relaciones_usuarios.obtenerDatosPerfilAmigo.
      replace("{{username}}", this.userSession.getUsername()).
      replace("{{usernameAmigo}}", usernameAmigo);

    this.http.setDataSerializer('json')
    return this.http.get(url, params, header);
  }
}
