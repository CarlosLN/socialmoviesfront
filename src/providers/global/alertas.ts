import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';


@Injectable()
export class Alertas {

    constructor(private toastCtrl: ToastController) { }

    mostarAlerta(mensaje: string, duracion?: number, posicion?: string) {
        let toast = this.toastCtrl.create({
            message: mensaje,
            duration: duracion ? duracion : 3000,
            position: posicion ? posicion : 'top',
            cssClass: 'normalToast'
        });

        toast.onDidDismiss(() => {
            //console.log('Dismissed toast');
        });

        toast.present();
    }

    mostarAlertaError(mensaje: string, duracion?: number, posicion?: string) {
        let toast = this.toastCtrl.create({
            message: mensaje,
            duration: duracion ? duracion : 3000,
            position: posicion ? posicion : 'top',
            cssClass: 'errorToast'
        });

        toast.onDidDismiss(() => {
            //console.log('Dismissed toast');
        });

        toast.present();
    }

    
}
