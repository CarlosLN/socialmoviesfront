import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, Events } from 'ionic-angular';

import { User, Alertas } from '../../providers';
import { MainPage } from '../';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loginForm: FormGroup

  account: { username: string, password: string } = {
    username: '',
    password: ''
  };

  // Our translated text strings
  private paginasLogin;
  private erroresLogin;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    private alertas: Alertas,
    public translateService: TranslateService,
    public events: Events) {

    this.cargarTraducciones()
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  // Attempt to login in through our User service
  doLogin() {
    this.user.login(this.account).then(data => {
      this.user.setToken("" + JSON.parse(data.data).body.token);
      this.user.setAvatar("" + JSON.parse(data.data).body.avatar);
      this.user.setUsername(this.account.username);
      this.events.publish('actualizar:user', this.user);
      this.alertas.mostarAlerta(this.paginasLogin.SESION_INICIADA, 3000, 'bottom');
      this.navCtrl.setRoot(MainPage);
    })
      .catch(error => {
        console.log(error)
        this.alertas.mostarAlertaError("Se ha producido un error haciendo login");
      });
  }

  private cargarTraducciones() {
    this.translateService.get('PAGINAS.LOGIN').subscribe((value) => {
      this.paginasLogin = value;
    })
    this.translateService.get('ERRORES').subscribe((value) => {
      this.erroresLogin = value;
    })
  }
}
