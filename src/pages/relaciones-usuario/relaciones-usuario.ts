import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RelacionesUsuariosProvider } from '../../providers/relaciones-usuarios/relUsuarios';
import { ViewChild } from '@angular/core';
import { Searchbar } from 'ionic-angular'

@IonicPage()
@Component({
  selector: 'page-relaciones-usuario',
  templateUrl: 'relaciones-usuario.html',
})
export class RelacionesUsuarioPage {

  @ViewChild('busquedaUsuario') searchbar: Searchbar;

  peticionesAmistadRecibidas = [];
  peticionesAmistadEnviadas = [];
  amistades = [];
  amistadesOriginal = [];
  usuarioBuscado: string = "";
  usuariosBuscados: Array<any> = new Array<any>();

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _relacionesUsuaios: RelacionesUsuariosProvider) {
  }

  ionViewWillEnter() {
    console.log("Estoy aqui")
    this.peticionesAmistadRecibidas = [];
    this.peticionesAmistadEnviadas = [];
    this.amistades = [];
    this.amistadesOriginal = [];
    this.obtenerPeticionesAmistadRecibidas();
    this.obtenerPeticionesAmistadEnviadas();
    this.obtenerAmistades();
  }

  obtenerPeticionesAmistadRecibidas() {
    this._relacionesUsuaios.obtenerPeticionesAmistad().then(data => {
      let respuesta: Array<any> = JSON.parse(data.data).body;
      this.peticionesAmistadRecibidas = respuesta;
    })
      .catch(error => {

        //console.log("Error Obteniendo Peticiones", error);
      });
  }

  obtenerPeticionesAmistadEnviadas() {
    this._relacionesUsuaios.obtenerPeticionesAmistadEnviadas().then(data => {
      let respuesta: Array<any> = JSON.parse(data.data).body;
      this.peticionesAmistadEnviadas = respuesta;
    })
      .catch(error => {

        //console.log("Error Obteniendo Peticiones", error);
      });
  }

  obtenerAmistades() {
    this._relacionesUsuaios.obtenerAmistades().then(data => {
      let respuesta: Array<any> = JSON.parse(data.data).body;
      this.amistades = respuesta;
      this.amistades.sort(function (a, b) { //Los ordeno alfabéticamente
        return a.username.localeCompare(b.username);
      });
      this.amistadesOriginal = this.amistades;
    })
      .catch(error => {
        //console.log("Error Obteniendo Amistades", error);
      });
  }

  actualizarPagina() {
    this.peticionesAmistadRecibidas = [];
    this.obtenerPeticionesAmistadRecibidas()
    this.peticionesAmistadEnviadas = [];
    this.obtenerPeticionesAmistadEnviadas();
    this.amistades = [];
    this.amistadesOriginal = [];
    this.obtenerAmistades();


    this.usuarioBuscado = "";
    this.usuariosBuscados = new Array<any>();
    this.searchbar.value = "";
  }

  filtrarAmistades(evento) {
    this.amistades = this.amistadesOriginal;

    let val = evento.target.value;

    if (val && val.trim() != '') {
      this.amistades = this.amistades.filter((item) => {
        return (item.username.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  buscarUsuarios(evento) {
    this.usuarioBuscado = "" + evento.target.value

    this._relacionesUsuaios.buscarUsuario(evento.target.value).then(data => {
      this.usuariosBuscados = new Array<any>();
      let respuesta: Array<any> = JSON.parse(data.data).body;

      this.usuariosBuscados = respuesta
    })
      .catch(error => {
        this.usuariosBuscados = new Array<any>();
      });
  }

  limpiarFiltroBusquedaAmistades(evento) {
    this.obtenerAmistades();
  }

  limpiarFiltroBusquedaUsuarios() {
    this.usuarioBuscado = ""
    this.usuariosBuscados = new Array<any>();
  }

}
