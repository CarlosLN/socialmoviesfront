import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RelacionesUsuarioPage } from './relaciones-usuario';
import { PeticionAmistadRecibidaModule } from '../../components/peticion-amistad-recibida/peticion-amistad-recibida.module';
import { ItemAmigoModule } from '../../components/item-amigo/item-amigo.module';
import { TranslateModule } from '@ngx-translate/core';
import { PeticionAmistadEnviadaModule } from '../../components/peticion-amistad-enviada/peticion-amistad-enviada.module';
import { ItemBusquedaUsuarioModule } from '../../components/item-busqueda-usuario/item-busqueda-usuario.module';

@NgModule({
  declarations: [
    RelacionesUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(RelacionesUsuarioPage),
    TranslateModule.forChild(),
    PeticionAmistadRecibidaModule,
    PeticionAmistadEnviadaModule,
    ItemBusquedaUsuarioModule,
    ItemAmigoModule
  ],
})
export class RelacionesUsuarioPageModule {}
