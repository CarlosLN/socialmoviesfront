import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Searchbar, Events, ModalController } from 'ionic-angular';
import { SeriesProvider } from '../../providers/series/series';

/**
 * Generated class for the SeriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-series',
  templateUrl: 'series.html',
})
export class SeriesPage {

  @ViewChild('busquedaSeries') searchbar: Searchbar;

  readonly cuantasPorPagina = 1;
  readonly cuantasSeriesBuscadasPorPagina = 5;
  serieBuscada: string = "";
  seriesBuscadas: any = new Array<any>();
  serieRecomendada: any;
  serieMejorValorada: any;
  serieMasVotada: any;
  paginaSerieMejorValorada = 0;
  paginaSerieMasVotada = 0;
  bloquearBotonSiguienteMasVotada = false
  bloquearBotonSiguienteMejorValorada = false
  paginaSerieBuscada = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _providerSeries: SeriesProvider,
    public modalCtrl: ModalController,
    public events: Events) {

    events.subscribe('actualizar:votacionSerie', () => {
      this.traerSerieRecomendada();
      this.traerSerieMejorValorada();
      this.traerSerieMasVotada();
    });

    this.traerSerieRecomendada();
    this.traerSerieMejorValorada();
    this.traerSerieMasVotada();
  }

  buscarSeries(evento) {
    if (evento.target != undefined) {
      this.serieBuscada = "" + evento.target.value
      this.paginaSerieBuscada = 0
    } else {
      this.serieBuscada = evento
    }


    if (this.serieBuscada.length >= 2) {
      let paginaBuscadas
      this.paginaSerieBuscada != 0 ? paginaBuscadas = this.paginaSerieBuscada - 1 : paginaBuscadas = 0;
      this._providerSeries.buscarSeries(this.serieBuscada, paginaBuscadas, this.cuantasSeriesBuscadasPorPagina).then(data => {
        this.seriesBuscadas = JSON.parse(data.data).body
        console.log("serie buscada:", JSON.parse(data.data).body)
      })
        .catch(error => {
          //TODO error
          console.log(error)
        });
    }

  }

  cambiarPaginaSeriessBuscadas(pagina) {
    this.paginaSerieBuscada = pagina;
    this.buscarSeries(this.serieBuscada)
  }

  limpiarFiltroBusquedaSerie() {
    this.serieBuscada = "";
    this.seriesBuscadas = new Array<any>();
    this.paginaSerieBuscada = 0;
  }


  abrirSerie(serie) {
    let serieModal = this.modalCtrl.create("SeriePage", { serie: serie });
    serieModal.present();
  }

  traerSerieRecomendada() {
    if (this.serieRecomendada == null) {
      this._providerSeries.obtenerSerieRecomendada(0).then(data => {
        this.serieRecomendada = JSON.parse(data.data).body;
        console.log("Serie recomendada1:", this.serieRecomendada)
      })
        .catch(error => {
          //TODO error
          console.log(error)
        });
    } else {
      this._providerSeries.obtenerSerieRecomendada(this.serieRecomendada.idSerie).then(data => {
        this.serieRecomendada = JSON.parse(data.data).body;
        console.log("Serie recomendada2:", this.serieRecomendada)
      })
        .catch(error => {
          //TODO error
          console.log(error)
        });
    } 
  }

  traerSerieMejorValorada() {
    this._providerSeries.obtenerSerieMejorValorada(this.paginaSerieMejorValorada, this.cuantasPorPagina).then(data => {
      if (JSON.parse(data.data).body != null) {
        this.serieMejorValorada = JSON.parse(data.data).body;
        this.bloquearBotonSiguienteMejorValorada = false;
      } else {
        this.paginaSerieMejorValorada--;
        this.bloquearBotonSiguienteMejorValorada = true;
      }
      console.log("Serie Mejor valorada:", this.serieMejorValorada)
    })
      .catch(error => {
        //TODO error
        console.log(error)
      });
  }

  anteriorSiguienteSerieMejorValorada(anteriorSiguiente) {
    if (anteriorSiguiente == 'anterior') {
      this.paginaSerieMejorValorada--;
    } else {
      this.paginaSerieMejorValorada++;
    }
    this.traerSerieMejorValorada();
  }

  traerSerieMasVotada() {
    this._providerSeries.obtenerSerieMasVotada(this.paginaSerieMasVotada, this.cuantasPorPagina).then(data => {
      if (JSON.parse(data.data).body != null) {
        this.serieMasVotada = JSON.parse(data.data).body;
        this.bloquearBotonSiguienteMasVotada = false;
      } else {
        this.paginaSerieMasVotada--;
        this.bloquearBotonSiguienteMasVotada = true;
      }
      console.log("Serie mas votada:", this.serieMasVotada)
    })
      .catch(error => {
        //TODO error
      });
  }

  anteriorSiguienteSerieMasVotada(anteriorSiguiente) {
    if (anteriorSiguiente == 'anterior') {
      this.paginaSerieMasVotada--;
    } else {
      this.paginaSerieMasVotada++;
    }
    this.traerSerieMasVotada();
  }

}
