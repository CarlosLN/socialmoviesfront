import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeriesPage } from './series';
import { NgxPaginationModule } from 'ngx-pagination';
import { PipesModule } from '../../pipes/pipes.module';
import { ItemSerieBuscadaModule } from '../../components/item-serie-buscada/item-serie-buscada.module';

@NgModule({
  declarations: [
    SeriesPage,
  ],
  imports: [
    IonicPageModule.forChild(SeriesPage),
    ItemSerieBuscadaModule,
    NgxPaginationModule,
    PipesModule
  ],
})
export class SeriesPageModule {}
