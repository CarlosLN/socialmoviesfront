import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, Events } from 'ionic-angular';
import { JuegosProvider } from '../../providers/juegos/juegos';

/**
 * Generated class for the JuegosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-juegos',
  templateUrl: 'juegos.html',
})
export class JuegosPage {

  ventana = "ranking";

  rankingDeJugadores = [];
  paginaRankingJugadores = 0;
  cuantosJugadores = 10;

  listadoDeJuegos = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _providerJuego: JuegosProvider,
    public modalCtrl: ModalController,
    public events: Events,
    private alertCtrl: AlertController) {
    this.traerRankingDeJugadores();

    //TODO evento para que actualice  el ranking de jugadores y el listado de juegos cuando un usuario juegue a uno
    events.subscribe('actualizar:Juegos', () => {
      this.traerRankingDeJugadores();
      this.devolverTodosLosJuegos();
    });
  }

  traerRankingDeJugadores() {
    this.paginaRankingJugadores = 0;
    this._providerJuego.obtenerRankingTotalDeUsuariosEnJuegos(this.paginaRankingJugadores, this.cuantosJugadores).then(data => {
      if (JSON.parse(data.data).body.length != 0) {
        this.rankingDeJugadores = JSON.parse(data.data).body;
        this.paginaRankingJugadores++;
        console.log("Ranking de los jugadores:", this.rankingDeJugadores)
      } else {
        this.rankingDeJugadores = null;
        console.log("Ranking de los jugadores vacío:", this.rankingDeJugadores)
      }

    })
      .catch(error => {
        //TODO error
        console.log(error)
        this.rankingDeJugadores = null;
      });
  }


  devolverTodosLosJuegos() {
    this._providerJuego.obtenerListadoDeJuegosConMisParticipaciones().then(data => {
      this.listadoDeJuegos = JSON.parse(data.data).body;
      console.log("Lista de juegos:", this.listadoDeJuegos)
    })
      .catch(error => {
        //TODO error
        console.log(error)
      });
  }


  abrirUnJuego(juego) {

    let peliculaModal = this.modalCtrl.create("JuegoPage", { 
      juego: juego
    });
    peliculaModal.present();


   /*  this.navCtrl.push("JuegoPage", {
      juego: juego
    }); */
  }

  mostrarReglas() {
    let alert = this.alertCtrl.create({
      title: 'Reglas del juego',
      cssClass: 'miAlerta',
      message: 'La puntuación obtenida en cada juego dependerá de la dificultad del mismo así como del tiempo que tardes en contestar cada una de las preguntas. Las preguntas falladas no sumarán puntos.<br/>'+
      '<br/>En caso de que abandones el juego antes de finalizarlo, tan solo obtendrás la puntuación de las preguntas respondidas correctamente.<br/><br/>'+
      'Una vez finalizado el juego, no podrás volver a participar en él.',
      //TODO escribir las reglas del juego
      buttons: ['Cerrar']
    });
    alert.present();
  }


  doUsuariosInfinite(scrollInfinito): Promise<any> {
    let rankingJuga;
    return new Promise((resolve) => {
      this._providerJuego.obtenerRankingTotalDeUsuariosEnJuegos(this.paginaRankingJugadores, this.cuantosJugadores).then(data => {
        rankingJuga = JSON.parse(data.data).body;
        if (rankingJuga == null) {
          scrollInfinito.enable(false)
        } else {
          Array.prototype.push.apply(this.rankingDeJugadores, rankingJuga)
          this.paginaRankingJugadores++;
        }

        resolve();
      })
        .catch(error => {
          console.log("Error cargando el timeline 1", error)
          resolve();
        });
    })
  }

}
