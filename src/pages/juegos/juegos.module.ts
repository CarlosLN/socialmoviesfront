import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JuegosPage } from './juegos';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    JuegosPage,
  ],
  imports: [
    IonicPageModule.forChild(JuegosPage),
    PipesModule
  ],
})
export class JuegosPageModule {}
