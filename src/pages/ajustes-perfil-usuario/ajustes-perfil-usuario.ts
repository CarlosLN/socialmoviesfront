import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, Events } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Alertas, User } from '../../providers';
import * as Globals from '../../providers/global/globals'

/**
 * Generated class for the AjustesPerfilUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ajustes-perfil-usuario',
  templateUrl: 'ajustes-perfil-usuario.html',
})
export class AjustesPerfilUsuarioPage {

  traduccionesPerfil: any;
  avatarAntiguo = "";
  avatar = "";
  username = "";
  cambioPassForm: FormGroup;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private actionSheetCtrl: ActionSheetController,
    public translateService: TranslateService,
    private camera: Camera,
    public events: Events,
    private alertas: Alertas,
    private alertCtrl: AlertController,
    public _providerUsuario: User,) {

    this.username = navParams.get('username');
    this.avatar = navParams.get('avatar');

    this.cambioPassForm = new FormGroup({
      passNueva: new FormControl('', [Validators.required, Validators.pattern(Globals.PASSWORDPATTERM)]),
      passRepetir: new FormControl('', [Validators.required]),
      passActual: new FormControl('', [Validators.required]),
    });

    this.translateService.get('PAGINAS.PERFIL').subscribe((value) => {
      this.traduccionesPerfil = value;
    })
  }

  public abirDialogoModificarFoto(): void {
    this.actionSheetCtrl.create({
      title: this.traduccionesPerfil.SELECCIONAR_FOTO_PERFIL,
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.traduccionesPerfil.FOTO_GALERIA,
          handler: () => this.seleccionarFoto(true)
        },
        {
          text: this.traduccionesPerfil.FOTO_CAMARA,
          handler: () => this.seleccionarFoto(false)
        },
        {
          text: this.traduccionesPerfil.CANCELAR
        }
      ]
    }).present();
  }

  private seleccionarFoto(album: boolean): void {
    let source: number = this.camera.PictureSourceType.CAMERA;

    if (album) {
      source = this.camera.PictureSourceType.SAVEDPHOTOALBUM;
    }

    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: source,
      targetWidth: 400,
      targetHeight: 400,
      allowEdit: true,
      saveToPhotoAlbum: true
    }

    this.camera.getPicture(options)
      .then(imageData => {
        this.avatarAntiguo = this.avatar;
        this.avatar = 'data:image/png;base64,' + imageData
        setTimeout(() => {
          this.cambiarAvatar();
        }, 500);
      }, error => {
        this.alertas.mostarAlertaError(this.traduccionesPerfil.ERROR_OBTENIENDO_FOTO);
      });
  }

  private cambiarAvatar() {
    let alert = this.alertCtrl.create({
      title: this.traduccionesPerfil.CAMBIO_AVATAR,
      message: this.traduccionesPerfil.DESEAS_CAMBIAR_AVATAR,
      buttons: [
        {
          text: this.traduccionesPerfil.ACEPTAR,
          handler: () => {
            this._providerUsuario.actualizarUsuario("avatar", this.avatar).then((valor) => {
              this._providerUsuario.setAvatar(this.avatar)
              this.events.publish('actualizar:avatar', this.avatar);
              this.alertas.mostarAlerta(this.traduccionesPerfil.AVATAR_MODIFICADO, 3000, "bottom");
            }).catch((error) => {
              this.alertas.mostarAlertaError(this.traduccionesPerfil.AVATAR_NO_MODIFICADO, 3000, "top");
              console.log(error)
            })
          }
        },
        {
          text: this.traduccionesPerfil.CANCELAR,
          role: 'cancel',
          handler: () => {
            this.avatar = this.avatarAntiguo;
          }
        }
      ]
    });
    alert.present();
  }


  darDeBajaUnUsuario(){
    let alert = this.alertCtrl.create({
      title: "¿Eliminar Usuario?",
      message: "Si lo haces, el usuario se eliminará y perderás los datos",
      buttons: [
        {
          text: this.traduccionesPerfil.ACEPTAR,
          handler: () => {
            this._providerUsuario.darDeBajaUnUsuario().then((valor) => {
              this._providerUsuario.logout();
              this.navCtrl.setRoot('WelcomePage');
              this.alertas.mostarAlerta("El usuario se ha eliminado correctamente", 3000, "bottom");
            }).catch((error) => {
              this.alertas.mostarAlertaError(this.traduccionesPerfil.AVATAR_NO_MODIFICADO, 3000, "Usuario dado de baja");
            })
          }
        },
        {
          text: this.traduccionesPerfil.CANCELAR,
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


  //TODO TRADUCCIONES
  cambiarPassword() {

    if (this.cambioPassForm.value.passNueva == this.cambioPassForm.value.passRepetir) {

      let alert = this.alertCtrl.create({
        title: this.traduccionesPerfil.CAMBIO_DE_PASS,
        message: this.traduccionesPerfil.DESEA_CAMBIO_DE_PASS,
        buttons: [
          {
            text: this.traduccionesPerfil.ACEPTAR,
            handler: () => {
              this._providerUsuario.cambiarPassword(this.cambioPassForm.value.passNueva, this.cambioPassForm.value.passActual).then((valor) => {
                this.alertas.mostarAlerta(this.traduccionesPerfil.PASS_ACTUALIZADA, 3000, "bottom");
                this.cambioPassForm.reset();
              }).catch((error) => {
                if (JSON.parse(error.error).code == 601) {
                  this.alertas.mostarAlertaError(this.traduccionesPerfil.PASS_NO_COINCIDE, 3000, "top");
                } else {
                  this.alertas.mostarAlertaError(this.traduccionesPerfil.PASS_NO_CAMBIADA, 3000, "top");
                }
              })
            }
          },
          {
            text: this.traduccionesPerfil.CANCELAR,
            role: 'cancel',
            handler: () => {
            }
          }
        ]
      });
      alert.present();
    } else {
      this.alertas.mostarAlertaError(this.traduccionesPerfil.PASS_NO_IGUALES, 3000, "top");
    }
  }

}
