import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AjustesPerfilUsuarioPage } from './ajustes-perfil-usuario';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    AjustesPerfilUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(AjustesPerfilUsuarioPage),
    PipesModule,
    IonicImageViewerModule,
    TranslateModule.forChild(),
  ],
})
export class AjustesPerfilUsuarioPageModule {}
