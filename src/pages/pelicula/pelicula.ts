import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, Platform, Events, AlertController } from 'ionic-angular';
import { PeliculasProvider } from '../../providers/peliculas/peliculas';
import { DomSanitizer } from '@angular/platform-browser';
import { PopoverController } from 'ionic-angular';
import { VotacionPopoverComponent } from '../../components/votacion-popover/votacion-popover';
import { Alertas } from '../../providers';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the PeliculaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pelicula',
  templateUrl: 'pelicula.html',
})
export class PeliculaPage {

  readonly cuantasVotacionesAmigosPorPagina = 3;
  readonly cuantosComentariosAmigosPorPagina = 3;
  paginaActualPeliculasVotadas = 0;
  paginaActualPeliculasComentario = 0;
  pelicula: any;
  peliculaTraida: any = {};
  mostrarTrailer: any = false;
  unregisterBackButtonAction: any;

  constructor(public viewCtrl: ViewController, public navParams: NavParams,
    public _peliculaService: PeliculasProvider,
    public platform: Platform,
    private alertas: Alertas,
    public events: Events,
    private alertCtrl: AlertController,
    private inAppBrowse: InAppBrowser,
    public popoverCtrl: PopoverController,
    private dom: DomSanitizer) {
    this.pelicula = navParams.get('pelicula');
    console.log("pelicula", this.pelicula);
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.dismiss();
    });
    this.obtenerDatosPeliculas(this.pelicula.idPelicula)
  }

  //Este metodo es necesario para volver a tener el normal funcionamiento del boton del back
  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    this.events.publish('actualizar:timeLine');
  }

  dismiss() {
    if (this.peliculaTraida.tuVoto == -1) {
      this.peliculaTraida.fechatuVoto = null;
    }
    this.viewCtrl.dismiss({ pelicula: this.peliculaTraida })
  }

  sanitize(vid) {
    return this.dom.bypassSecurityTrustResourceUrl(vid);
  }

  cambiarMostrarTrailer() {
    this.mostrarTrailer = !this.mostrarTrailer;
  }

  obtenerDatosPeliculas(idPelicula) {
    this._peliculaService.obtenerPeliculaUsuario(idPelicula).then((valor) => {
      this.peliculaTraida = JSON.parse(valor.data).body
      console.log("peliculaTraida", this.peliculaTraida)
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }

  
  votar(myEvent) {
    let popover = this.popoverCtrl.create(VotacionPopoverComponent);
    popover.present();
    popover.onDidDismiss(popoverDate => {
      if (popoverDate != null) {
        this._peliculaService.votarPelicula(this.pelicula.idPelicula, popoverDate.valor).then((valor) => {
          this.peliculaTraida.tuVoto = popoverDate.valor;
          this.obtenerDatosPeliculas(this.pelicula.idPelicula);
          this.events.publish('actualizar:votacionPelicula');
          if (popoverDate.valor == -1) {
            this.alertas.mostarAlerta("No has visto esta película", 2000);
          } else {
            this.alertas.mostarAlerta("Has votado esta película con un "+popoverDate.valor, 2000);
          }
        }).catch(error => {
          //TODO CATCH
          console.log("Fallo aqui", error);
        })
      }
    })
  }

  cambiarPaginaVotacionesDeAmigo(event){
    this.paginaActualPeliculasVotadas = event;
    this.obtenerVotacionesDeAmigosPaginados();
  }

  obtenerVotacionesDeAmigosPaginados(){
    let paginaAmigosVotacion
    this.paginaActualPeliculasVotadas != 0 ? paginaAmigosVotacion = this.paginaActualPeliculasVotadas -1 : paginaAmigosVotacion = 0; 
    this._peliculaService.obtenerVotacionesDeAmigosDeUnaPelicula(this.peliculaTraida.id, paginaAmigosVotacion, this.cuantasVotacionesAmigosPorPagina).then((valor) => {
      this.peliculaTraida.votacionesAmigos = JSON.parse(valor.data).body;
    }).catch(error=>{
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }


  comentarPelicula() {
      let alert = this.alertCtrl.create({
        title: 'Escribe tu comentario',
        inputs: [
          {
            name: 'comentario',
            type: 'textarea',
            value: this.peliculaTraida.miComentario
          }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: data => {
            }
          },
          {
            text: 'Guardar',
            handler: data => {
              var comentario = data.comentario.replace('"',"'")
              this._peliculaService.comentarPelicula(this.pelicula.idPelicula,comentario).then((valor) => {
                this.peliculaTraida.miComentario = comentario;
                if (comentario.length <= 0) {
                  this.alertas.mostarAlerta("Pelicula no comentada", 2000);
                } else {
                  this.alertas.mostarAlerta("Has comentado esta película", 2000);
                }
                this.events.publish('actualizar:comentarioPelicula');
              }).catch(error => {
                //TODO CATCH
                console.log("Fallo aqui", error);
              });
            }
          }
        ]
      });
      alert.present();
  }

  cambiarPaginaComentariosDeAmigos(event){
    this.paginaActualPeliculasComentario = event;
    this.obtenerComentariosDeAmigosPaginados();
  }

  obtenerComentariosDeAmigosPaginados(){
    
    let paginaAmigosComentario
    this.paginaActualPeliculasComentario != 0 ? paginaAmigosComentario = this.paginaActualPeliculasComentario -1 : paginaAmigosComentario = 0; 
    this._peliculaService.obtenerComentariosDeAmigosDeUnaPelicula(this.peliculaTraida.id, paginaAmigosComentario, this.cuantosComentariosAmigosPorPagina).then((valor) => {
      this.peliculaTraida.comentariosAmigos = JSON.parse(valor.data).body;
    }).catch(error=>{
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }

  abrirDistribuidora(url){
    this.inAppBrowse.create(url, '_system');
  }



}
