import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PeliculaPage } from './pelicula';
import { PipesModule } from '../../pipes/pipes.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { ItemAmigoVotadoPeliculaModule } from '../../components/item-amigo-votado-pelicula/item-amigo-votado-pelicula.module';
import { ItemAmigoComentarioPeliculaModule } from '../../components/item-amigo-comentario-pelicula/item-amigo-comentario-pelicula.module';

@NgModule({
  
  declarations: [
    PeliculaPage
  ],
  imports: [
    IonicPageModule.forChild(PeliculaPage),
    PipesModule,
    NgxPaginationModule,
    IonicImageViewerModule,
    ItemAmigoVotadoPeliculaModule,
    ItemAmigoComentarioPeliculaModule
  ],
})
export class PeliculaPageModule { }
