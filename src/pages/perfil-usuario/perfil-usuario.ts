import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, Events, List } from 'ionic-angular';
import { Camera, } from '@ionic-native/camera';
import { Alertas, User } from '../../providers';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import * as Globals from '../../providers/global/globals'
import { PeliculasProvider } from '../../providers/peliculas/peliculas';
import { RelacionesUsuariosProvider } from '../../providers/relaciones-usuarios/relUsuarios';
import { SeriesProvider } from '../../providers/series/series';



@IonicPage()
@Component({
  selector: 'page-perfil-usuario',
  templateUrl: 'perfil-usuario.html',
})

export class PerfilUsuarioPage {
  readonly cuantaspeliculasVotadasPorPagina = 5;
  readonly cuantasSeriesVotadasPorPagina = 5;
  readonly cuantaspeliculasComentadasPorPagina = 5;
  readonly cuantasSeriesComentadasPorPagina = 5;
  botonSeleccionado = "pelisVotadas"
  username = ""
  tipoUsuario = ""
  avatar = ""
  traduccionesRegistro: any;
  traduccionesPerfil: any;
  traducciones: any;
  traduccionesPeticionAmistad: any;
  //peliculasSeriesVisible = "peliculas"
  //peliculasVotadasComentadosVisible = "votadas"
  //seriesVotadasComentadosVisible = "votadas"
  peliculasVotadas: Array<any> = [];
  seriesVotadas: Array<any> = [];
  peliculasComentadas: Array<any> = [];
  seriesComentadas: Array<any> = [];
  mostarBotonMasPeliculas = true;
  motrarMensajeNoHay = false;
  datosPerfilUsuario: any = {};

  // Peliculas votadas
  paginaPeliculasVotadas = 0;
  ordenPeliculasVotadas = "FechaVotacionDesc";

  // Series votadas
  paginaSeriesVotadas = 0;
  ordenSeriesVotadas = "FechaVotacionDesc";

  // Peliculas comentadas
  paginaPeliculasComentadas = 0;
  ordenPeliculasComentadas = "FechaComentarioDesc"

  // Series cometnadas
  paginaSeriesComentadas = 0;
  ordenSeriesComentadas = "FechaComentarioDesc"

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertas: Alertas,
    private alertCtrl: AlertController,
    public translateService: TranslateService,
    public _providerUsuario: User,
    public _relacionesUsuario: RelacionesUsuariosProvider,
    public _providerPeliculas: PeliculasProvider,
    public _providerSeries: SeriesProvider,
    public events: Events) {

    this.username = navParams.get('username');
    this.avatar = navParams.get('avatar');
    this.tipoUsuario = navParams.get('tipoUsuario');


    events.subscribe('actualizar:avatar', (avatar) => {
      console.log("Pruebas cambiando", avatar)
      this.avatar = avatar
    });

    events.subscribe('actualizar:votacionPelicula', () => {
      this.obtenerDatosPerfil();
      this.obtenerPeliculasDeUnUsuario();
    });

    events.subscribe('actualizar:votacionSerie', () => {
      this.obtenerDatosPerfil();
      this.obtenerSeriesDeUnUsuario();
    });

    events.subscribe('actualizar:comentarioPelicula', () => {
      this.obtenerDatosPerfil();
      this.obtenerPeliculasComentadasDeUnUsuario();
    });

    events.subscribe('actualizar:comentarioSerie', () => {
      this.obtenerDatosPerfil();
      this.obtenerSeriesComentadasDeUnUsuario();
    });

    this.translateService.get('PAGINAS.REGISTRO').subscribe((value) => {
      this.traduccionesRegistro = value;
    })

    this.translateService.get('PAGINAS.PERFIL').subscribe((value) => {
      this.traduccionesPerfil = value;
    })

    this.translateService.get('COMPONENTS.ITEM_AMIGO').subscribe((value) => {
      this.traducciones = value;
    })

    this.translateService.get('COMPONENTS.PETICION_AMISTAD_RECIBIDA').subscribe((value) => {
      this.traduccionesPeticionAmistad = value;
    })


    //this.paginaPeliculasVotadas = 0;
    //this.datosPerfilUsuario.numPeliculasVotadas=0;

    this.obtenerDatosPerfil();

    this.obtenerPeliculasDeUnUsuario()

  }

  /**
   * Administracion perfil del usuario
   */
  private obtenerDatosPerfil() {
    this._relacionesUsuario.obtenerDatosPerfilAmigo(this.username).then((valor) => {
      this.datosPerfilUsuario = JSON.parse(valor.data).body;
      console.log("Datos perfil usuario", this.datosPerfilUsuario)
      this.datosPerfilUsuario.numPeliculasVotadas = JSON.parse(valor.data).body.numPeliculasVotadas;
    }).catch((error) => {
      //TODO CATCH
    })
  }

  public eliminarAmistad() {
    let mensaje = this.traducciones.CONFIRMAR_ELIMINACION.replace("{{username}}", this.username)

    let alert = this.alertCtrl.create({
      title: this.traducciones.CONFIRMAR_ELIMINACION_TITULO,
      message: mensaje,
      buttons: [
        {
          text: this.traducciones.ACEPTAR,
          handler: () => {
            this._relacionesUsuario.aceptarRechazarCancelarPeticionAmistad(this.username, "eliminarAmistad").then((valor) => {
              let mensajeAlerta = this.traducciones.AMISTAD_ELIMINADA.replace("{{username}}", this.username)
              this.events.publish('eliminarAmigo', this.username);
              this.alertas.mostarAlerta(mensajeAlerta, 2000);
              this.navCtrl.remove(this.navCtrl.length() - 1);
            }).catch((error) => {
              console.log("Error Eliminando amistad", error)
            })
          }
        },
        {
          text: this.traducciones.CANCELAR,
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  public aceptarDenegarPeticion(accion: string) {
    let mensaje = ""

    if (accion == "aceptarPeticion") {
      mensaje = this.traduccionesPeticionAmistad.ACEPTAR_PETICION
    } else {
      mensaje = this.traduccionesPeticionAmistad.RECHAZAR_PETICION
    }

    mensaje = mensaje.replace("{{username}}", this.username)

    let alert = this.alertCtrl.create({
      title: this.traduccionesPeticionAmistad.SOLICITUD_AMISTAD,
      message: mensaje,
      buttons: [
        {
          text: this.traduccionesPeticionAmistad.ACEPTAR,
          handler: () => {
            this._relacionesUsuario.aceptarRechazarCancelarPeticionAmistad(this.username, accion).then((valor) => {
              if (accion == "aceptarPeticion") {
                this.alertas.mostarAlerta(this.traduccionesPeticionAmistad.PETICION_ACEPTADA.replace('{{username}}', this.username), 2000);
                this.tipoUsuario = "amigo"
              } else {
                this.alertas.mostarAlertaError(this.traduccionesPeticionAmistad.PETICION_RECHAZADA, 2000);
                this.navCtrl.pop();
              }
            }).catch((error) => {
              console.log("Error Peticion aceptada", error)
            })
          }
        },
        {
          text: this.traduccionesPeticionAmistad.CANCELAR,
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  irAEditarMiPerfil() {
    this.navCtrl.push("AjustesPerfilUsuarioPage", {
      username: this.username,
      avatar: this.avatar
    });
  }

  irAlListadoDeAmigos() {
    if (this.tipoUsuario == 'usuarioActual') {
      this.navCtrl.push("RelacionesUsuarioPage");
    } else {
      this.navCtrl.push("ListadoAmigosUsuarioPage",{
        amigoUsername: this.username,
        cuantosAmigosTiene: this.datosPerfilUsuario.numAmigos
      });
    }
  }


  cambiarListadoDePeliculasSeries(evento) {
    this.botonSeleccionado = evento;
    switch (evento) {
      case "pelisComentadas":
        this.obtenerPeliculasComentadasDeUnUsuario();
        break;
      case "seriesVotadas":
        this.obtenerSeriesDeUnUsuario();
        break;
      case "seriesComentadas":
        this.obtenerSeriesComentadasDeUnUsuario();
        break;
      default:
        this.obtenerPeliculasDeUnUsuario();
        break;
    }
  }


  /**
   * Peliculas Votadas
   */

  obtenerPeliculasDeUnUsuario() {
    let paginavotacion
    this.paginaPeliculasVotadas != 0 ? paginavotacion = this.paginaPeliculasVotadas - 1 : paginavotacion = 0;
    this._providerPeliculas.obtenerPeliculasVotadasDeUnUsuario(this.username, this.ordenPeliculasVotadas, paginavotacion, this.cuantaspeliculasVotadasPorPagina).then((valor) => {
      this.peliculasVotadas = JSON.parse(valor.data).body
      this.peliculasVotadas.length == 0 ? this.motrarMensajeNoHay = true : this.motrarMensajeNoHay = false;
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }

  cambiarOrdenListaPeliculasVotadas() {
    this.paginaPeliculasVotadas = 0;
    this.obtenerPeliculasDeUnUsuario()
  }

  cambiarPaginaPeliculasVotadas(evento) {
    this.paginaPeliculasVotadas = evento;
    this.obtenerPeliculasDeUnUsuario();
  }

  /**
   * Series Votadas
   */

  obtenerSeriesDeUnUsuario(){
    let paginavotacion;
    this.paginaSeriesVotadas != 0 ? paginavotacion = this.paginaSeriesVotadas -1 : paginavotacion = 0;
    this._providerSeries.obtenerSeriesVotadasDeUnUsuario(this.username, this.ordenSeriesVotadas, paginavotacion, this.cuantasSeriesVotadasPorPagina).then((valor) => {
      this.seriesVotadas = JSON.parse(valor.data).body
      this.seriesVotadas.length == 0 ? this.motrarMensajeNoHay = true : this.motrarMensajeNoHay = false;
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }

  cambiarOrdenListaSeriesVotadas(){
    this.paginaSeriesVotadas = 0;
    this.obtenerSeriesDeUnUsuario();
  }

  cambiarPaginaSeriesVotadas(evento){
    this.paginaSeriesVotadas = evento;
    this.obtenerSeriesDeUnUsuario();
  }

  /**
   * Peliculas Comentadas
   */
  obtenerPeliculasComentadasDeUnUsuario() {
    let paginacomentarios
    this.paginaPeliculasComentadas != 0 ? paginacomentarios = this.paginaPeliculasComentadas - 1 : paginacomentarios = 0;
    this._providerPeliculas.obtenerPeliculasComentadasDeUnUsuario(this.username, this.ordenPeliculasComentadas, paginacomentarios, this.cuantaspeliculasComentadasPorPagina).then((valor) => {
      this.peliculasComentadas = JSON.parse(valor.data).body
      this.peliculasComentadas.length == 0 ? this.motrarMensajeNoHay = true : this.motrarMensajeNoHay = false;
      console.log(this.peliculasComentadas)
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }

  cambiarOrdenListaPeliculasComentadas() {
    this.paginaPeliculasComentadas = 0;
    this.obtenerPeliculasComentadasDeUnUsuario();
  }

  cambiarPaginaPeliculasComentadas(evento) {
    this.paginaPeliculasComentadas = evento;
    this.obtenerPeliculasComentadasDeUnUsuario();
  }

/**
   * Series Comentadas
   */
  obtenerSeriesComentadasDeUnUsuario(){
    let paginacomentarios
    this.paginaSeriesComentadas != 0 ? paginacomentarios = this.paginaSeriesComentadas - 1 : paginacomentarios = 0;
    this._providerSeries.obtenerCapituloSerieComentadosDeUnUsuario(this.username, this.ordenSeriesComentadas, paginacomentarios, this.cuantasSeriesComentadasPorPagina).then((valor) => {
      this.seriesComentadas = JSON.parse(valor.data).body
      this.seriesComentadas.length == 0 ? this.motrarMensajeNoHay = true : this.motrarMensajeNoHay = false;
      console.log(this.seriesComentadas)
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }

  cambiarOrdenListaSeriesComentadas() {
    this.paginaSeriesComentadas = 0;
    this.obtenerSeriesComentadasDeUnUsuario();
  }

  cambiarPaginaSeriesComentadas(evento) {
    this.paginaSeriesComentadas = evento;
    this.obtenerSeriesComentadasDeUnUsuario();
  }


}
