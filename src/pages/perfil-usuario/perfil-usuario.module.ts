import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerfilUsuarioPage } from './perfil-usuario';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { PeliculaDeUnUsuarioModule } from '../../components/pelicula-de-un-usuario/pelicula-de-un-usuario.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { PeliculaComentadaDeUnUsuarioModule } from '../../components/pelicula-comentada-de-un-usuario/pelicula-comentada-de-un-usuario.module';
import { SerieDeUnUsuarioModule } from '../../components/serie-de-un-usuario/serie-de-un-usuario.module';
import { SerieComentadaDeUnUsuarioModule } from '../../components/serie-comentada-de-un-usuario/serie-comentada-de-un-usuario.module';


@NgModule({
  declarations: [
    PerfilUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(PerfilUsuarioPage),
    PipesModule,
    NgxPaginationModule,
    IonicImageViewerModule,
    TranslateModule.forChild(),
    PeliculaDeUnUsuarioModule,
    SerieDeUnUsuarioModule,
    PeliculaComentadaDeUnUsuarioModule,
    SerieComentadaDeUnUsuarioModule
  ],
})
export class PerfilUsuarioPageModule {}
