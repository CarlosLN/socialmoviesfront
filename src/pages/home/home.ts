import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Content, Events } from 'ionic-angular';
import { RelacionesUsuariosProvider } from '../../providers/relaciones-usuarios/relUsuarios';
import { User } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild(Content) content: Content;

  peticionesAmistadRecibidas = 0
  fechaPrimerElemento: any
  timeLineActual: any
  fechaUltimoElemento: any = null
  timeLineRecibido

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _relacionesUsuaios: RelacionesUsuariosProvider,
    public _userProvider: User,
    public events: Events,
    private menu: MenuController) {

    events.subscribe('actualizar:timeLine', () => {
      this.obtenerTimeLineUsuario("antes");
      this.peticionesAmistadRecibidas = 0
      this.obtenerPeticionesAmistadRecibidas();
    });

    

    this.obtenerTimeLineUsuario("ahora");
    this.menu.swipeEnable(true);
  }

  ionViewWillEnter() {
    console.log("Entro en ionViewWillEnter")
    if (this.timeLineActual != null) {
      this.obtenerTimeLineUsuario("antes");
    }
    this.peticionesAmistadRecibidas = 0
    this.obtenerPeticionesAmistadRecibidas();

    this.events.subscribe('actualizar:avatar', (avatar) => {
      this.timeLineActual.forEach(element => {
        if (element.nombreUsuario == this._userProvider.getUsername()) { element.imagenUser = avatar }
      });
    });

    this.events.subscribe('eliminarAmigo', (nombreAmigo) => {
      console.log('this.timeLineActual',this.timeLineActual)
      let longitudTimelineActual = this.timeLineActual.length;
      for (let index = 0; index < longitudTimelineActual; index++) {
        if (this.timeLineActual[index].nombreUsuario == nombreAmigo) {
          this.timeLineActual.splice(index, 1);
          longitudTimelineActual = this.timeLineActual.length;
          index = 0;
        }
      }
    });
  }

  private obtenerPeticionesAmistadRecibidas() {
    this._relacionesUsuaios.obtenerPeticionesAmistad().then(data => {
      let respuesta: Array<any> = JSON.parse(data.data).body;
      this.peticionesAmistadRecibidas = respuesta.length
    })
      .catch(error => {

      });
  }

  doInfinite(scrollInfinito): Promise<any> {
    console.log("CARGANDO TIMELINEEE ABAJO")
    return new Promise((resolve) => {
      this._userProvider.obtenerTimeLine(this.fechaPrimerElemento, this.fechaUltimoElemento, "despues").then(data => {
        this.timeLineRecibido = JSON.parse(data.data).body;
        if (this.timeLineRecibido == null) {
          scrollInfinito.enable(false)
        } else {
          this.fechaPrimerElemento = this.timeLineRecibido.fechaPrimerElemento
          this.fechaUltimoElemento = this.timeLineRecibido.fechaUltimoElemento
          Array.prototype.push.apply(this.timeLineActual, this.timeLineRecibido.elementList)
        }
        resolve();
      })
        .catch(error => {
          console.log("Error cargando el timeline 1", error)
          resolve();
        });
    })
  }

  doRefresh(refresher): Promise<any> {
    console.log("CARGANDO TIMELINEEE ARRIBA")
    return new Promise((resolve) => {
      this._userProvider.obtenerTimeLine(this.fechaPrimerElemento, this.fechaUltimoElemento, "antes").then(data => {
        if (JSON.parse(data.data).body != null) {
          this.timeLineRecibido = JSON.parse(data.data).body;
          if (this.timeLineRecibido != null) {
            this.fechaPrimerElemento = this.timeLineRecibido.fechaPrimerElemento
            this.fechaUltimoElemento = this.timeLineRecibido.fechaUltimoElemento
            this.timeLineActual = this.timeLineRecibido.elementList.concat(this.timeLineActual)
          }
        }
        refresher.complete();
      })
        .catch(error => {
          console.log("Error cargando el timeline 2", error)
          refresher.complete();
        });
    })
  }

  obtenerTimeLineUsuario(direccionScroll: any) {
    this._userProvider.obtenerTimeLine(this.fechaPrimerElemento, this.fechaUltimoElemento, direccionScroll).then(data => {
      if (JSON.parse(data.data).body != null) {
        this.timeLineRecibido = JSON.parse(data.data).body;
        if (this.timeLineRecibido != null) {
          this.fechaPrimerElemento = this.timeLineRecibido.fechaPrimerElemento
          this.fechaUltimoElemento = this.timeLineRecibido.fechaUltimoElemento
        }
        if (direccionScroll == "antes") {
          //this.verificarEliminarEventosDuplicados(this.timeLineRecibido)
          this.timeLineActual = this.timeLineRecibido.elementList.concat(this.timeLineActual)
          //this.eliminarEventosDuplicados()
        } else if (direccionScroll == "despues") {
          //this.verificarEliminarEventosDuplicados(this.timeLineRecibido)
          Array.prototype.push.apply(this.timeLineActual, this.timeLineRecibido.elementList)
          //this.eliminarEventosDuplicados()
        } else {
          this.timeLineActual = this.timeLineRecibido.elementList
        }
        console.log("TimeLineActual", this.timeLineActual, "TimeLineRecibido", this.timeLineRecibido)
      }
    })
      .catch(error => {
        console.log("Error cargando el timeline 3", error)
      });
  }

  abrirRelacionesUsuarios() {
    this.navCtrl.push("RelacionesUsuarioPage");
    console.log("TimeLineActual", this.timeLineActual, "TimeLineRecibido", this.timeLineRecibido)
  }

  recargarInicio() {
    try {
      this.obtenerTimeLineUsuario("antes");
      this.obtenerPeticionesAmistadRecibidas();
      this.content.scrollTo(0, 0)
    } catch (error) {
      console.log("Error haciendo scroll top", error)
    }
  }

  abrirPaginaPeliculas() {
    this.navCtrl.push("PeliculasPage");
  }

  abrirPaginaSeries() {
    this.navCtrl.push("SeriesPage");
  }

  abrirPaginaJuegoRetos() {
    this.navCtrl.push("JuegosPage");
  }







}
