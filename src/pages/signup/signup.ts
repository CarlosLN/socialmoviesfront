import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ActionSheetController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { User, Alertas } from '../../providers';
import { MainPage } from '../';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Events } from 'ionic-angular';
import * as Globals from '../../providers/global/globals'

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signupform: FormGroup;

  account: { username: string, email: string, password: string, avatar: string } = {
    username: '',
    email: '',
    password: '',
    avatar: null
  };

  repetirPass: string = "";

  traduccionesRegistro: any;
  avatar = '../../assets/img/avatar.png'

  constructor(public navCtrl: NavController,
    public user: User,
    private camera: Camera,
    private alertas: Alertas,
    private actionSheetCtrl: ActionSheetController,
    public translateService: TranslateService,
    public events: Events) {

    this.translateService.get('PAGINAS.REGISTRO').subscribe((value) => {
      this.traduccionesRegistro = value;
    })
  }

  ngOnInit() {
    

    this.signupform = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern(Globals.USERNAMEPATTERN)]),
      password: new FormControl('', [Validators.required, Validators.pattern(Globals.PASSWORDPATTERM)]),
      email: new FormControl('', [Validators.required, Validators.pattern(Globals.EMAILPATTERN)]),
      repetirPass: new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
  }

  doSignup() {

    if (this.account.password == this.repetirPass) {
      this.account.avatar = this.avatar;
      this.user.signup(this.account).then(data => {
        this.user.setToken("" + JSON.parse(data.data).body.token);
        this.user.setAvatar(this.avatar)
        this.user.setUsername(this.account.username);
        this.events.publish('actualizar:user', this.user);
        this.alertas.mostarAlerta(this.traduccionesRegistro.REGISTRO_CORRECTO);
        this.navCtrl.setRoot(MainPage);
      })
        .catch(error => {
          this.alertas.mostarAlertaError(this.traduccionesRegistro.ERROR_REGISTRO);
        });
    } else {
      this.alertas.mostarAlertaError(this.traduccionesRegistro.PASS_DISTINTAS);
    }

  }

  public abirDialogoModificarFoto(): void {
    this.actionSheetCtrl.create({
      title: this.traduccionesRegistro.SELECCIONAR_FOTO_PERFIL,
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.traduccionesRegistro.FOTO_GALERIA,
          handler: () => this.seleccionarFoto(true)
        },
        {
          text: this.traduccionesRegistro.FOTO_CAMARA,
          handler: () => this.seleccionarFoto(false)
        },
        {
          text: this.traduccionesRegistro.CANCELAR
        }
      ]
    }).present();
  }

  private seleccionarFoto(album: boolean): void {
    let source: number = this.camera.PictureSourceType.CAMERA;

    if (album) {
      source = this.camera.PictureSourceType.SAVEDPHOTOALBUM;
    }

    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: source,
      targetWidth: 400,
      targetHeight: 400,
      allowEdit: true,
      saveToPhotoAlbum: true
    }

    this.camera.getPicture(options)
      .then(imageData => {
        this.avatar = 'data:image/png;base64,' + imageData
      }, error => {
        this.alertas.mostarAlertaError(this.traduccionesRegistro.ERROR_OBTENIENDO_FOTO);
      });
  }
}
