import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Searchbar, ModalController, Events } from 'ionic-angular';
import { PeliculasProvider } from '../../providers/peliculas/peliculas';

/**
 * Generated class for the PeliculasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-peliculas',
  templateUrl: 'peliculas.html',
})
export class PeliculasPage {

  @ViewChild('busquedaPeliculas') searchbar: Searchbar;

  readonly cuantasPorPagina = 1;
  readonly cuantasPeliculasBuscadasPorPagina = 5;
  peliculaBuscada: string = "";
  peliculasBuscadas: any = new Array<any>();
  peliculaRecomendada: any;
  peliculaMejorValorada: any;
  peliculaMasVotada: any;
  paginaPeliculaMejorValorada = 0;
  paginaPeliculaMasVotada = 0;
  bloquearBotonSiguienteMasVotada = false
  bloquearBotonSiguienteMejorValorada = false
  paginaPeliculaBuscada = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _providerPeliculas: PeliculasProvider,
    public modalCtrl: ModalController,
    public events: Events) {

    events.subscribe('actualizar:votacionPelicula', () => {
      this.traerPeliculaRecomendada();
      this.traerPeliculaMejorValorada();
      this.traerPeliculaMasVotada();
    });

    this.traerPeliculaRecomendada();
    this.traerPeliculaMejorValorada();
    this.traerPeliculaMasVotada();

  }


  buscarPeliculas(evento) {
    if (evento.target != undefined) {
      this.peliculaBuscada = "" + evento.target.value
      this.paginaPeliculaBuscada = 0
    } else {
      this.peliculaBuscada = evento
    }


    if (this.peliculaBuscada.length >= 2) {
      let paginaBuscadas
      this.paginaPeliculaBuscada != 0 ? paginaBuscadas = this.paginaPeliculaBuscada - 1 : paginaBuscadas = 0;
      this._providerPeliculas.buscarPelicula(this.peliculaBuscada, paginaBuscadas, this.cuantasPeliculasBuscadasPorPagina).then(data => {
        this.peliculasBuscadas = JSON.parse(data.data).body
        console.log("Pelicula buscada:", JSON.parse(data.data).body)
      })
        .catch(error => {
          //TODO error
          console.log(error)
        });
    }

  }

  cambiarPaginaPeliculasBuscadas(pagina) {
    this.paginaPeliculaBuscada = pagina;
    this.buscarPeliculas(this.peliculaBuscada)
  }

  limpiarFiltroBusquedaPelicula() {
    this.peliculaBuscada = "";
    this.peliculasBuscadas = new Array<any>();
    this.paginaPeliculaBuscada = 0;
  }

  abrirPelicula(pelicula) {
    let peliculaModal = this.modalCtrl.create("PeliculaPage", { pelicula: pelicula });
    peliculaModal.present();
  }

  traerPeliculaRecomendada() {
    if (this.peliculaRecomendada == null) {
      this._providerPeliculas.obtenerPeliculaRecomendada(0).then(data => {
        this.peliculaRecomendada = JSON.parse(data.data).body;
        console.log("Pelicula recomendada1:", this.peliculaRecomendada)
      })
        .catch(error => {
          //TODO error
          console.log(error)
        });
    } else {
      this._providerPeliculas.obtenerPeliculaRecomendada(this.peliculaRecomendada.idPelicula).then(data => {
        this.peliculaRecomendada = JSON.parse(data.data).body;
        console.log("Pelicula recomendada2:", this.peliculaRecomendada)
      })
        .catch(error => {
          //TODO error
          console.log(error)
        });
    }
  }

  traerPeliculaMejorValorada() {
    this._providerPeliculas.obtenerPeliculaMejorValorada(this.paginaPeliculaMejorValorada, this.cuantasPorPagina).then(data => {
      if (JSON.parse(data.data).body != null) {
        this.peliculaMejorValorada = JSON.parse(data.data).body;
        this.bloquearBotonSiguienteMejorValorada = false;
      } else {
        this.paginaPeliculaMejorValorada--;
        this.bloquearBotonSiguienteMejorValorada = true;
      }
      console.log("Pelicula Mejor valorada:", this.peliculaMejorValorada)
    })
      .catch(error => {
        //TODO error
        console.log(error)
      });
  }

  anteriorSiguientePeliculaMejorValorada(anteriorSiguiente) {
    if (anteriorSiguiente == 'anterior') {
      this.paginaPeliculaMejorValorada--;
    } else {
      this.paginaPeliculaMejorValorada++;
    }
    this.traerPeliculaMejorValorada();
  }

  traerPeliculaMasVotada() {
    this._providerPeliculas.obtenerPeliculaMasVotada(this.paginaPeliculaMasVotada, this.cuantasPorPagina).then(data => {
      if (JSON.parse(data.data).body != null) {
        this.peliculaMasVotada = JSON.parse(data.data).body;
        this.bloquearBotonSiguienteMasVotada = false;
      } else {
        this.paginaPeliculaMasVotada--;
        this.bloquearBotonSiguienteMasVotada = true;
      }
      console.log("Pelicula mas votada:", this.peliculaMasVotada)
    })
      .catch(error => {
        //TODO error
      });
  }

  anteriorSiguientePeliculaMasVotada(anteriorSiguiente) {
    if (anteriorSiguiente == 'anterior') {
      this.paginaPeliculaMasVotada--;
    } else {
      this.paginaPeliculaMasVotada++;
    }
    this.traerPeliculaMasVotada();
  }






}
