import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PeliculasPage } from './peliculas';
import { PipesModule } from '../../pipes/pipes.module';
import { ItemPeliculaBuscada } from '../../components/item-pelicula-buscada/item-pelicula-buscada.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    PeliculasPage,
  ],
  imports: [
    IonicPageModule.forChild(PeliculasPage),
    ItemPeliculaBuscada,
    NgxPaginationModule,
    PipesModule
  ],
})
export class PeliculasPageModule {}
