import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JuegoPage } from './juego';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    JuegoPage,
  ],
  imports: [
    IonicPageModule.forChild(JuegoPage),
    PipesModule
  ],
})
export class JuegoPageModule {}
