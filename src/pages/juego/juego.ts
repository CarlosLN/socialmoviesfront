import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, ViewController, Events } from 'ionic-angular';
import { JuegosProvider } from '../../providers/juegos/juegos';
import { Alertas } from '../../providers';

/**
 * Generated class for the JuegoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-juego',
  templateUrl: 'juego.html',
})
export class JuegoPage {

  mostrarBotonEmpezarJuego = false;
  jugando = false;
  mostrarMiResultado = false;
  mostarMisRespuestas = false;

  juego;
  juegoCompletoTraido;
  numeroDePreguntaMostrada = 0;

  downloadTimer: any;
  temporizador = 15;
  misRespuestas = [];
  opcionesDeRespuestasDivididas;

  unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _providerJuego: JuegosProvider,
    public viewCtrl: ViewController,
    private alertCtrl: AlertController,
    public events: Events,
    public platform: Platform,
    private alertas: Alertas) {
    this.juego = navParams.get('juego');
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      if (this.jugando) {
        let alert = this.alertCtrl.create({
          title: "¿Deseas salir del juego?",
          message: "Si lo haces solo obtendrás la puntuación de las preguntas respondidas",
          buttons: [
            {
              text: "Aceptar",
              handler: () => {
                clearInterval(this.downloadTimer);
                this.enviarMisRespuestas(true);
              }
            },
            {
              text: "Cancelar",
              role: 'cancel',
              handler: () => {
              }
            }
          ]
        });
        alert.present();
      } else {
        this.viewCtrl.dismiss()

      }
    });
    this.traerElJuego();
    console.log('Juego', this.juego);
  }

  //Este metodo es necesario para volver a tener el normal funcionamiento del boton del back
  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  traerElJuego() {
    this._providerJuego.obtenerJuegoParaElUsuario(this.juego.idJuego).then(data => {
      this.juegoCompletoTraido = JSON.parse(data.data).body;
      this.dividirLasOpcionesDeRespuesta();
      if (this.juegoCompletoTraido.preguntasJuego != null && this.juegoCompletoTraido.respuestasJuegos == null) {
        this.mostrarBotonEmpezarJuego = true;
      } else {
        this.misRespuestas = this.juegoCompletoTraido.respuestasJuegos
        this.mostarMisRespuestas = true;
      }
      console.log("Juego completo traido", this.juegoCompletoTraido)
    })
      .catch(error => {
        //TODO error
        console.log(error)
      });
  }

  iniciarJuego() {
    this.jugando = true;
    this.mostrarBotonEmpezarJuego = false;
    this.iniciarCuentaAtras();
  }

  responderPregunta(respuesta) {

    clearInterval(this.downloadTimer);

    this.misRespuestas.push({
      "idPreguntaJuego": this.juegoCompletoTraido.preguntasJuego[this.numeroDePreguntaMostrada].id,
      "tiempoTardado": 16 - this.temporizador,
      "respuesta": respuesta,
      "respuestaCorrecta": this.juegoCompletoTraido.preguntasJuego[this.numeroDePreguntaMostrada].respuestaCorrecta
    })
    this.temporizador = 1; //Pongo esto aquí para que pare el intervale de la cuenta atrás
    if (this.juegoCompletoTraido.preguntasJuego.length - this.numeroDePreguntaMostrada != 1) {
      this.siguientePregunta();
      this.iniciarCuentaAtras();
    } else {
      this.enviarMisRespuestas(false);
    }
  }

  siguientePregunta() {
    this.numeroDePreguntaMostrada++;
    this.dividirLasOpcionesDeRespuesta();
  }

  preguntaAnterior() {
    this.numeroDePreguntaMostrada--;
    this.dividirLasOpcionesDeRespuesta();
  }

  dividirLasOpcionesDeRespuesta() {
    this.opcionesDeRespuestasDivididas = this.juegoCompletoTraido.preguntasJuego[this.numeroDePreguntaMostrada].opciones.split(".");
  }

  enviarMisRespuestas(abandono: boolean) {
    if (abandono) {
      for (let index = this.numeroDePreguntaMostrada; index < this.juegoCompletoTraido.preguntasJuego.length; index++) {
        this.misRespuestas.push({
          "idPreguntaJuego": this.juegoCompletoTraido.preguntasJuego[index].id,
          "tiempoTardado": 15,
          "respuesta": "",
          "respuestaCorrecta": this.juegoCompletoTraido.preguntasJuego[index].respuestaCorrecta
        })
      }
    }

    this._providerJuego.participarEnJuego(this.misRespuestas).then(data => {
      //TODO guardar mi participacion
      this.events.publish('actualizar:Juegos');
      if (abandono) {
        this.viewCtrl.dismiss()
      } else {
        this.alertas.mostarAlerta("Has obtenido " + JSON.parse(data.data).body + " puntos");
        this.mostrarMiResultado = true;
      }
    })
      .catch(error => {
        //TODO error
        console.log(error)
      });


    this.jugando = false;
    console.log("Mis respuestas", this.misRespuestas)
  }

  iniciarCuentaAtras() {
    this.temporizador = 15;
    this.downloadTimer = setInterval(() => {
      if (this.temporizador <= 1) {
        this.responderPregunta("");
      } else {
        this.temporizador -= 1;
      }
    }, 1000, 15);
  }


  mostarResultado() {
    this.numeroDePreguntaMostrada = 0;
    this.dividirLasOpcionesDeRespuesta()
    this.mostrarMiResultado = false;
    this.mostarMisRespuestas = true;
  }

  obtenerElColorDelBoton(valorDeLaOpcion) {
    if (this.misRespuestas[this.numeroDePreguntaMostrada].respuesta == "") { //Si no se ha respondido a esta pregunta
      return "primary";
    } else {
      if (this.misRespuestas[this.numeroDePreguntaMostrada].respuesta == this.misRespuestas[this.numeroDePreguntaMostrada].respuestaCorrecta) { //Si la respuesta es correcta
        if (this.misRespuestas[this.numeroDePreguntaMostrada].respuesta == valorDeLaOpcion) { //Si la respuesta es la opcion que esta siendo valorada
          return "secondary"
        } else { //Si la respuesta no es la opcion que esta siendo valorada
          return "primary"
        }
      } else { //Si la respuesta no es correcta
        if (this.misRespuestas[this.numeroDePreguntaMostrada].respuesta == valorDeLaOpcion) { //Si la respuesta es la opcion valorada
          return "danger"
        } else { //Si la respuesta no es la opcion valorada
          if (valorDeLaOpcion == this.misRespuestas[this.numeroDePreguntaMostrada].respuestaCorrecta) {
            return "secondary"
          } else {
            return "primary"
          }
        }
      }
    }
  }

  respuestaOutline(valorDeLaOpcion) {
    if (this.misRespuestas[this.numeroDePreguntaMostrada].respuesta == "") { //Si no se ha respondido a esta pregunta
      if (valorDeLaOpcion == this.misRespuestas[this.numeroDePreguntaMostrada].respuestaCorrecta) {
        return false
      } else {
        return true
      }
    } else {
      if (this.misRespuestas[this.numeroDePreguntaMostrada].respuesta == this.misRespuestas[this.numeroDePreguntaMostrada].respuestaCorrecta) { //Si la respuesta es correcta
        if (this.misRespuestas[this.numeroDePreguntaMostrada].respuesta == valorDeLaOpcion) { //Si la respuesta es la opcion que esta siendo valorada
          return false
        } else { //Si la respuesta no es la opcion que esta siendo valorada
          return true
        }
      } else { //Si la respuesta no es correcta
        if (this.misRespuestas[this.numeroDePreguntaMostrada].respuesta == valorDeLaOpcion) { //Si la respuesta es la opcion valorada
          return false
        } else { //Si la respuesta no es la opcion valorada
          if (valorDeLaOpcion == this.misRespuestas[this.numeroDePreguntaMostrada].respuestaCorrecta) {
            return false
          } else {
            return true
          }
        }
      }
    }
  }


}
