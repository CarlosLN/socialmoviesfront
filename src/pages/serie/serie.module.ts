import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeriePage } from './serie';
import { PipesModule } from '../../pipes/pipes.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { ItemAmigoVotadoSerieModule } from '../../components/item-amigo-votado-serie/item-amigo-votado-serie.module';
import { ItemAmigoComentarioSerieModule } from '../../components/item-amigo-comentario-serie/item-amigo-comentario-serie.module';

@NgModule({
  declarations: [
    SeriePage,
  ],
  imports: [
    IonicPageModule.forChild(SeriePage),
    PipesModule,
    NgxPaginationModule,
    IonicImageViewerModule,
    ItemAmigoVotadoSerieModule,
    ItemAmigoComentarioSerieModule
  ],
})
export class SeriePageModule {}
