import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform, Events, AlertController, PopoverController, Content } from 'ionic-angular';
import { SeriesProvider } from '../../providers/series/series';
import { Alertas } from '../../providers';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DomSanitizer } from '@angular/platform-browser';
import { VotacionPopoverComponent } from '../../components/votacion-popover/votacion-popover';

/**
 * Generated class for the SeriePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-serie',
  templateUrl: 'serie.html',
})
export class SeriePage {

  @ViewChild(Content) content: Content;

  readonly cuantasVotacionesAmigosPorPagina = 3;
  readonly cuantosComentariosAmigosPorPagina = 3;
  paginaActualSeriesVotadas = 0;
  paginaActualSeriesComentario = 0;
  serie: any;
  serieTraida: any = {};
  mostrarTrailer: any = false;
  unregisterBackButtonAction: any;
  temporadasDesplegadas: any;

  constructor(public viewCtrl: ViewController, public navParams: NavParams,
    public _seriesService: SeriesProvider,
    public platform: Platform,
    private alertas: Alertas,
    public events: Events,
    private alertCtrl: AlertController,
    private inAppBrowse: InAppBrowser,
    public popoverCtrl: PopoverController,
    private dom: DomSanitizer
  ) {
    this.serie = navParams.get('serie');
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.dismiss();
    });
    this.obtenerDatosSeries(this.serie.idSerie)
  }

  //Este metodo es necesario para volver a tener el normal funcionamiento del boton del back
  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    this.events.publish('actualizar:timeLine');
  }

  dismiss() {
    if (this.serieTraida.tuVoto == -1) {
      this.serieTraida.fechatuVoto = null;
    }
    this.viewCtrl.dismiss({ serie: this.serieTraida })
  }

  sanitize(vid) {
    return this.dom.bypassSecurityTrustResourceUrl(vid);
  }

  pulsarEnBotonVotar(){
    this.content.scrollTo(0,250,500)
    this.mostrarOcultarCapitulos(0);
  }

  cambiarMostrarTrailer() {
    this.mostrarTrailer = !this.mostrarTrailer;
  }

  obtenerDatosSeries(idSerie) {
    this._seriesService.obtenerSerieUsuario(idSerie).then((valor) => {
      this.serieTraida = JSON.parse(valor.data).body
      this.temporadasDesplegadas = []
      for (let index = 0; index < this.serieTraida.temporadas.length; index++) {
        this.temporadasDesplegadas[index] = false;
      }
      console.log("serieTraida", this.serieTraida)
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }


  mostrarOcultarCapitulos(index) {
    if (this.serieTraida.temporadas[index].capitulosTemporada.length == 0) {
      this._seriesService.obtenerTemporadaYSusCapitulosDeUnaSerie(this.serie.idSerie, this.serieTraida.temporadas[index].numTemporada).then((valor) => {
        this.serieTraida.temporadas[index].capitulosTemporada = JSON.parse(valor.data).body.capitulosTemporada
        console.log("capitulosTraidos", this.serieTraida)
        this.temporadasDesplegadas[index] = !this.temporadasDesplegadas[index];
      }).catch(error => {
        //TODO CATCH
        console.log("Fallo aqui", error);
      })
    } else {
      this.temporadasDesplegadas[index] = !this.temporadasDesplegadas[index]
    }
  }

  /* traerCapitulosDeUnaTemporada(index) {
    this._seriesService.obtenerTemporadaYSusCapitulosDeUnaSerie(this.serie.idSerie, this.serieTraida.temporadas[index].numTemporada).then((valor) => {
      this.serieTraida.temporadas[index].capitulosTemporada = JSON.parse(valor.data).body
      console.log("capitulosTraidos", JSON.parse(valor.data).body)
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  } */

  votar(numTemporada, capitulo) {
    let popover = this.popoverCtrl.create(VotacionPopoverComponent);
    popover.present();
    popover.onDidDismiss(popoverDate => {
      if (popoverDate != null) {
        this._seriesService.votarCapitulo(capitulo.id, popoverDate.valor).then((valoresActualizados) => {
          capitulo.tuVoto = popoverDate.valor;
          this.actualizarTrasVotacion(capitulo, JSON.parse(valoresActualizados.data).body)
          this.events.publish('actualizar:votacionSerie');
          if (popoverDate.valor == -1) {
            this.alertas.mostarAlerta("No has visto este capítulo", 2000);
          } else {
            this.alertas.mostarAlerta("Has votado este capítulo con un " + popoverDate.valor, 2000);
          }
        }).catch(error => {
          //TODO CATCH
          console.log("Fallo aqui", error);
        })
      }
    })
  }

  private actualizarTrasVotacion(capitulo, valoresActualizados) {
    console.log("Datos actualizados", capitulo, valoresActualizados)
    this.serieTraida.tuVoto = valoresActualizados.tuVotoMedioSerie
    this.serieTraida.numVotantes = valoresActualizados.numVotantesSerie
    this.serieTraida.votacionMedia = valoresActualizados.votacionMediaSerie
    capitulo.numVotaciones = valoresActualizados.numVotacionesCapitulo
    capitulo.votacionMedia = valoresActualizados.votacionMediaCapitulo
    capitulo.fechatuVoto = valoresActualizados.fechatuVoto

    this.serieTraida.temporadas.forEach(temporada => {
      if(temporada.numTemporada == valoresActualizados.numTemporada){
          temporada.numVotaciones = valoresActualizados.numVotacionesTemporada
          temporada.votacionMedia = valoresActualizados.votacionMediaTemporada
      }
    });

  }

  cambiarPaginaVotacionesDeAmigo(event) {
    this.paginaActualSeriesVotadas = event;
    this.obtenerVotacionesDeAmigosPaginados();
  }

  obtenerVotacionesDeAmigosPaginados() {
   let paginaAmigosVotacion
    this.paginaActualSeriesVotadas != 0 ? paginaAmigosVotacion = this.paginaActualSeriesVotadas -1 : paginaAmigosVotacion = 0; 
    this._seriesService.obtenerVotacionesDeAmigosDeUnaSerie(this.serieTraida.id, paginaAmigosVotacion, this.cuantasVotacionesAmigosPorPagina).then((valor) => {
      this.serieTraida.votacionesAmigos = JSON.parse(valor.data).body;
    }).catch(error=>{
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }

  comentarCapituloSerie(capitulo) {
    let alert = this.alertCtrl.create({
      title: 'Escribe tu comentario',
      inputs: [
        {
          name: 'comentario',
          type: 'textarea',
          value: capitulo.miComentario
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
          }
        },
        {
          text: 'Guardar',
          handler: data => {
            var comentario = data.comentario.replace('"', "'")
            this._seriesService.comentarCapituloSerie(capitulo.id, comentario).then((valor) => {
              capitulo.miComentario = comentario;
              if (comentario.length <= 0) {
                this.alertas.mostarAlerta("Capitulo no comentado", 2000);
              } else {
                this.alertas.mostarAlerta("Has comentado este capitulo", 2000);
              }
              this.events.publish('actualizar:comentarioSerie');
            }).catch(error => {
              //TODO CATCH
              console.log("Fallo aqui", error);
            });
          }
        }
      ]
    });
    alert.present();
  }

  cambiarPaginaComentariosDeAmigos(event) {
    this.paginaActualSeriesComentario = event;
    this.obtenerComentariosDeAmigosPaginados();
  }

  obtenerComentariosDeAmigosPaginados() {

    let paginaAmigosComentario
    this.paginaActualSeriesComentario != 0 ? paginaAmigosComentario = this.paginaActualSeriesComentario - 1 : paginaAmigosComentario = 0;
    this._seriesService.obtenerComentariosDeAmigosDeUnaSerie(this.serieTraida.id, paginaAmigosComentario, this.cuantosComentariosAmigosPorPagina).then((valor) => {
      this.serieTraida.comentariosAmigos = JSON.parse(valor.data).body;
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }



  abrirDistribuidora(url) {
    this.inAppBrowse.create(url, '_system');
  }

}
