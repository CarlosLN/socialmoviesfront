import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListadoAmigosUsuarioPage } from './listado-amigos-usuario';
import { TranslateModule } from '@ngx-translate/core';
import { PeticionAmistadRecibidaModule } from '../../components/peticion-amistad-recibida/peticion-amistad-recibida.module';
import { PeticionAmistadEnviadaModule } from '../../components/peticion-amistad-enviada/peticion-amistad-enviada.module';
import { ItemBusquedaUsuarioModule } from '../../components/item-busqueda-usuario/item-busqueda-usuario.module';
import { ItemAmigoModule } from '../../components/item-amigo/item-amigo.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ItemPropioUsuarioModule } from '../../components/item-propio-usuario/item-propio-usuario.module';

@NgModule({
  declarations: [
    ListadoAmigosUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(ListadoAmigosUsuarioPage),
    TranslateModule.forChild(),
    NgxPaginationModule,
    PeticionAmistadRecibidaModule,
    PeticionAmistadEnviadaModule,
    ItemBusquedaUsuarioModule,
    ItemAmigoModule,
    ItemPropioUsuarioModule
  ],
})
export class ListadoAmigosUsuarioPageModule {}
