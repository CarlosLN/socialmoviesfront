import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RelacionesUsuariosProvider } from '../../providers/relaciones-usuarios/relUsuarios';

/**
 * Generated class for the ListadoAmigosUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listado-amigos-usuario',
  templateUrl: 'listado-amigos-usuario.html',
})
export class ListadoAmigosUsuarioPage {

  amigoUsername: any;
  cuantosAmigosTiene
  paginaListadoAmigos = 0;
  cuantosAmigosPorPagina = 5;
  listadoAmigos: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public _relUsuarios:RelacionesUsuariosProvider) {
  }

  ngOnInit(){
    this.amigoUsername = this.navParams.get('amigoUsername');
    this.cuantosAmigosTiene = this.navParams.get('cuantosAmigosTiene');
    this.obtenerListadoDeAmigos()
  }

  obtenerListadoDeAmigos(){
    let paginaamigos
    this.paginaListadoAmigos != 0 ? paginaamigos = this.paginaListadoAmigos - 1 : paginaamigos = 0;
    this._relUsuarios.obtenerListadoAmigosDeUnAmigo(this.amigoUsername, paginaamigos, this.cuantosAmigosPorPagina).then((valor) => {
      this.listadoAmigos = JSON.parse(valor.data).body
      console.log(this.listadoAmigos)
    }).catch(error => {
      //TODO CATCH
      console.log("Fallo aqui", error);
    })
  }

  cambiarPaginaListadoAmigos(evento) {
    this.paginaListadoAmigos = evento;
    this.obtenerListadoDeAmigos();
  }

}
