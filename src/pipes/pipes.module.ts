import { NgModule } from '@angular/core';
import { SafePipe } from './safe/safe';
import { FechaPipe } from './fecha/fecha';
import { AcortarComentariosPipe } from './acortar-comentarios/acortar-comentarios';
import { NumeroTemporadaCapituloPipe } from './numero-temporada-capitulo/numero-temporada-capitulo';
import { HoraPipe } from './hora/hora';
@NgModule({
	declarations: [SafePipe,
    FechaPipe,
    AcortarComentariosPipe,
    NumeroTemporadaCapituloPipe,
    FechaPipe,
    HoraPipe],
	imports: [],
	exports: [SafePipe,
    FechaPipe,
    AcortarComentariosPipe,
    NumeroTemporadaCapituloPipe,
    FechaPipe,
    HoraPipe]
})
export class PipesModule {}