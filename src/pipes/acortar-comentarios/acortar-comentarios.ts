import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the AcortarComentariosPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'acortarComentarios',
})
export class AcortarComentariosPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string) {
    return value.substring(0,70);
  }
}
