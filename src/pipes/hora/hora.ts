import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the HoraPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'hora',
})
export class HoraPipe implements PipeTransform {
  
  transform(value: string) {
    var d: Date = new Date(value)
    var hora = "";
    //return d.getDate().toLocaleString() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
    hora += d.getHours().toString().length == 1 ? "0"+d.getHours() : d.getHours();
    hora += ":" + (d.getMinutes().toString().length == 1 ? "0"+d.getMinutes() : d.getMinutes());
    hora += ":" + (d.getSeconds().toString().length == 1 ? "0"+d.getSeconds() : d.getSeconds());

    return hora;
  }
}
