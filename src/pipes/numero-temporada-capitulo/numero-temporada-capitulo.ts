import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the NumeroTemporadaCapituloPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'numeroTemporadaCapitulo',
})
export class NumeroTemporadaCapituloPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string) {
    let valor = ""+value
    //console.log("Pipe episodio", value, valor)
    if(valor.length == 1){
      valor = "0"+valor
    }
    return valor;
  }
}
