import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FechaPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'fecha',
})
export class FechaPipe implements PipeTransform {

  transform(value: string) {
    var d: Date = new Date(value)
    var fecha = "";
    //return d.getDate().toLocaleString() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
    fecha += d.getDate().toLocaleString().length == 1 ? "0"+d.getDate().toLocaleString() : d.getDate().toLocaleString();
    fecha += "/" + ((d.getMonth()+1).toString().length == 1 ? "0"+(d.getMonth()+1) : (d.getMonth()+1));
    fecha += "/" + d.getFullYear();
    return fecha;
  }
}
